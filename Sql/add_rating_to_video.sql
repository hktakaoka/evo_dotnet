CREATE PROCEDURE add_rating_to_video(
    @videoid VARCHAR(32),
    @source VARCHAR(32),
    @value DECIMAL(5,2),
    @modified_by VARCHAR(32)
)
    AS
        BEGIN
            IF NOT EXISTS (SELECT videoid FROM videos WHERE videoid = @videoid)
            BEGIN
                DECLARE @err_message VARCHAR(32);
                SET @err_message = @videoid + ' doest not exist.';
                RAISERROR (@err_message, 16, 1);
            end

           DECLARE @ratingid INT,
               @created_time DATETIME;

           SET @created_time = (SELECT getdate());

           EXEC @ratingid = dbo.add_rating @source, @value, @modified_by;

           INSERT INTO rating_videos(ratingid, videoid, added, modified, modified_by)
                VALUES (@ratingid, @videoid, @created_time, @created_time, @modified_by);
        end;