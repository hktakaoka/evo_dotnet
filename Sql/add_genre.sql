CREATE PROCEDURE add_genre (
    @name VARCHAR(32),
    @modified_by VARCHAR(32)
) AS
    BEGIN
        DECLARE @id INT;

        SET @id = (SELECT genreid
        FROM genres
        WHERE name = @name);

        IF @id IS NULL
        BEGIN
            DECLARE @current_time DATETIME;

            SET @current_time = (
                SELECT getdate()
                );

            INSERT INTO genres(name, added, modified, modified_by)
            VALUES (@name, @current_time, @current_time, @modified_by);

            SET @id = (SELECT genreid
            FROM genres
            WHERE name = @name);
        end

        RETURN @id;
    end;
