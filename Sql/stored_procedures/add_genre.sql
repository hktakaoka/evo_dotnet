--CREATE OR ALTER if using right sql server...
IF (OBJECT_ID('dbo.add_genre', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE dbo.add_genre;
END
GO

CREATE PROCEDURE dbo.add_genre (
    @name VARCHAR(32),
    @modified_by VARCHAR(32)
)
AS
BEGIN		
	DECLARE @current_timestamp DATETIME = GETDATE();
	DECLARE @genre_ids TABLE (genre_id INT);
			
	INSERT INTO dbo.genres([name], added, modified, modified_by)
	OUTPUT inserted.genre_id 
    INTO @genre_ids (genre_id)
	SELECT @name, @current_timestamp, @current_timestamp, @modified_by
			
	SELECT genre_id as 'ID'
	FROM @genre_ids;
END;