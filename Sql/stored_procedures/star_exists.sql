--CREATE OR ALTER if using right sql server...
IF (OBJECT_ID('dbo.star_exists', 'P') IS NOT NULL)
BEGIN
	DROP PROCEDURE dbo.star_exists;
END
GO

CREATE PROCEDURE dbo.star_exists (
    @first_name VARCHAR(64),
    @last_name VARCHAR(64),
    @suffix VARCHAR(64),
    @middle_name VARCHAR(64)
) AS
BEGIN

	--We can not guarantee that there will only be one row returned.
	SELECT a.star_id, a.first_name, a.middle_name, a.last_name, a.suffix
	FROM dbo.stars a
	WHERE a.first_name IS NULL OR a.first_name = @first_name
	AND a.last_name IS NULL OR a.last_name = @last_name
	AND a.suffix IS NULL OR a.suffix = @suffix
	AND a.middle_name IS NULL OR a.middle_name = @middle_name;

END;
