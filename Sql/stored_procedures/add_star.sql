CREATE PROCEDURE dbo.add_star (
    @first_name VARCHAR(64),
    @middle_name VARCHAR(64) = NULL,
    @last_name VARCHAR(64),
    @suffix VARCHAR(64) = NULL,
    @role VARCHAR(16),
    @modified_by VARCHAR(32)
) AS
BEGIN
	DECLARE @current_timestamp DATETIME = GETDATE();
	DECLARE @role_id INT = (
		SELECT a.role_id
		FROM dbo.star_role a
		WHERE a.[role_id] = @role
	);

	BEGIN TRY
		BEGIN TRAN

			INSERT INTO dbo.star_role ([role], added, modified, modified_by)
			SELECT @role, @current_timestamp, @current_timestamp, @modified_by
			WHERE @role_id IS NULL;

			--We need to change our rules as:
			--	-a star can share the same name as another star...
			--	-a star can not have any roles yet or was planned to be in a role, but was removed
			--	-a star can go only be a nickname (Prince)
			--Until then, this may be flimsy.
			MERGE INTO dbo.stars
			AS TARGET
			USING
			(
				SELECT @first_name as 'first_name', @last_name AS 'last_name', @role_id AS 'role_id'
			) AS SOURCE
			--Whatever goes here should be apart of a nonclustered index.
			ON TARGET.first_name = SOURCE.first_name
			AND TARGET.last_name = SOURCE.last_name
			AND TARGET.role_id = @role_id
			WHEN MATCHED
			THEN UPDATE
				SET 
				TARGET.middle_name = @middle_name,
				TARGET.suffix = @suffix,
				TARGET.modified = @current_timestamp,
				TARGET.modified_by = @modified_by
			WHEN NOT MATCHED
			THEN INSERT (first_name, middle_name, last_name, suffix, role_id, added, modified, modified_by)
			VALUES(@first_name, @middle_name, @last_name, @suffix, @role_id, @current_timestamp, NULL, @modified_by);

			SELECT COALESCE(SCOPE_IDENTITY(), 0) AS 'id';
		COMMIT
	END TRY
	BEGIN CATCH
		IF XACT_STATE() != 0
		BEGIN
			ROLLBACK;
		END

		;THROW;
	END CATCH
END;