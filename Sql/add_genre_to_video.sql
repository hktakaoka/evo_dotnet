CREATE PROCEDURE add_genre_to_video (
    @videoid VARCHAR(32),
    @genre_name VARCHAR(32),
    @modified_by VARCHAR(64)
)
    AS
        BEGIN
            IF NOT EXISTS (SELECT videoid FROM videos WHERE videoid = @videoid)
            BEGIN
                DECLARE @err_message VARCHAR(32);
                SET @err_message = @videoid + ' doest not exist.';
                RAISERROR (@err_message, 16, 1);
            end

            DECLARE @genre_id INT,
                @created_time DATETIME;

            SET @created_time = (SELECT getdate());
            EXEC @genre_id = dbo.add_genre @genre_name, @modified_by;

            INSERT INTO genre_videos(genreid, videoid, added, modified, modified_by)
                VALUES (@genre_id, @videoid, @created_time, @created_time, @modified_by)
        end;