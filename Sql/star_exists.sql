CREATE PROCEDURE star_exists (
    @first_name VARCHAR(64),
    @last_name VARCHAR(64),
    @suffix VARCHAR(64),
    @middle_name VARCHAR(64)
) AS
    BEGIN

        DECLARE @id INT;

        IF @suffix IS NULL AND @middle_name IS NULL
            BEGIN
                SET @id = (
                    SELECT starid
                    FROM stars
                    WHERE first_name = @first_name
                        AND last_name = @last_name
                        AND suffix IS NULL
                        AND middle_name IS NULL
                )
                IF @id IS NOT NULL
                    BEGIN
                        RETURN @id
                    end
                end

        ELSE IF @suffix IS NOT NULL AND @middle_name IS NULL
            BEGIN
                SET @id = (
                    SELECT starid
                    FROM stars
                    WHERE first_name = @first_name
                        AND last_name = @last_name
                        AND suffix = @suffix
                        AND middle_name IS NULL
                )
                IF @id IS NOT NULL
                    BEGIN
                        RETURN @id
                    end
                end

        ELSE IF @suffix IS NULL AND @middle_name IS NOT NULL
            BEGIN
                SET @id = (
                    SELECT starid
                    FROM stars
                    WHERE first_name = @first_name
                        AND last_name = @last_name
                        AND suffix IS NULL
                        AND middle_name = @middle_name
                )
                IF @id IS NOT NULL
                    BEGIN
                        RETURN @id
                    end
                end

        ELSE
            BEGIN
                SET @id = (
                    SELECT starid
                    FROM stars
                    WHERE first_name = @first_name
                        AND last_name = @last_name
                        AND suffix = @suffix
                        AND middle_name = @middle_name
                )
                IF @id IS NOT NULL
                    BEGIN
                        RETURN @id
                    end
                ELSE
                    RETURN 0;
                end
    end;
