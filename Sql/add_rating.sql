CREATE PROCEDURE add_rating (
    @source VARCHAR(32),
    @value DECIMAL(5,2),
    @modified_by VARCHAR(32)
) AS
    BEGIN

        DECLARE @id INT;

        SET @id = ( SELECT ratingid
                    FROM ratings
                    WHERE source = @source
                        AND value = @value);

        IF @id IS NULL
        BEGIN
            DECLARE @current_time DATETIME;

            SET @current_time = (
                SELECT getdate()
                );

           INSERT INTO ratings(source, value, added, modified, modified_by)
            VALUES (@source, @value, @current_time, @current_time, @modified_by);

            SET @id = ( SELECT ratingid
                        FROM ratings
                        WHERE source = @source
                            AND value = @value);
        end
        RETURN @id;
    end;