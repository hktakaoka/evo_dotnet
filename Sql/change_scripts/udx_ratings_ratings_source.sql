IF EXISTS(
	SELECT 1
	FROM sys.indexes a
	WHERE a.name = N'ux_ratings_source_value'
	AND a.object_id = OBJECT_ID(N'dbo.ratings'))
BEGIN
	DROP INDEX ux_ratings_source_value ON dbo.ratings;
END

CREATE UNIQUE NONCLUSTERED INDEX ux_ratings_source_value
ON dbo.ratings (source, value);