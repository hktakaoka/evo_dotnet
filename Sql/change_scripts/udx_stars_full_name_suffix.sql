IF EXISTS(
	SELECT 1
	FROM sys.indexes a
	WHERE a.name = 'ux_stars_full_name_suffix'
	AND a.object_id = OBJECT_ID('dbo.stars '))
BEGIN
	DROP INDEX ux_stars_full_name_suffix ON dbo.stars;
END

CREATE UNIQUE NONCLUSTERED INDEX UX_stars_full_name_suffix 
ON dbo.stars (first_name, middle_name, last_name, suffix);