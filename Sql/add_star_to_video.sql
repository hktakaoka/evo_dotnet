CREATE PROCEDURE add_star_to_video (
    @videoid VARCHAR(32),
    @first_name VARCHAR(64),
    @middle_name VARCHAR(64) = NULL,
    @last_name VARCHAR(64),
    @suffix VARCHAR(64) = NULL,
    @role VARCHAR(32),
    @modified_by VARCHAR(32)
)
    AS
        BEGIN

            IF NOT EXISTS (SELECT videoid FROM videos WHERE videoid = @videoid)
            BEGIN
                DECLARE @err_message VARCHAR(32);
                SET @err_message = @videoid + ' doest not exist.';
                RAISERROR (@err_message, 16, 1);
            end

            DECLARE @created_time DATETIME,
                @starid INT;

            SET @created_time = (SELECT getdate());

            EXEC @starid = dbo.add_star @first_name = @first_name,
                @middle_name = @middle_name,
                @last_name = @last_name,
                @suffix = @suffix,
                @role = @role,
                @modified_by = @modified_by;

            INSERT INTO star_videos(starid, videoid, added, modified, modified_by)
                VALUES(@starid, @videoid, @created_time, @created_time, @modified_by);
        end;