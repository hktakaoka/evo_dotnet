IF (OBJECT_ID(N'dbo.genres', N'U') IS NOT NULL)
BEGIN
	DROP TABLE dbo.genres;
END

CREATE TABLE dbo.genres (
    genre_id INT NOT NULL IDENTITY (1,1),
    name VARCHAR(32) UNIQUE,
    added DATETIME,
    modified DATETIME,
    modified_by VARCHAR(32),

    CONSTRAINT pk_genres_genre_id PRIMARY KEY (genre_id)
);