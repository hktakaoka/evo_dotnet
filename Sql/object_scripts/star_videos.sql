IF (OBJECT_ID(N'dbo.star_videos', N'U') IS NOT NULL)
BEGIN
	DROP TABLE dbo.star_videos;
END

CREATE TABLE star_videos (
    star_id INT NOT NULL,
    video_id INT NOT NULL,
    added DATETIME,
    modified DATETIME,
    modified_by VARCHAR(32),

    CONSTRAINT pk_star_videos_star_id_video_id PRIMARY KEY (star_id, video_id),
	CONSTRAINT fk_star_videos_star_id FOREIGN KEY (star_id) REFERENCES stars (star_id)
);