IF (OBJECT_ID(N'dbo.genre_videos', N'U') IS NOT NULL)
BEGIN
	DROP TABLE dbo.genre_videos;
END
	
CREATE TABLE dbo.genre_videos (
    genre_id INT NOT NULL,
    video_id INT NOT NULL,
    added DATETIME,
    modified DATETIME,
    modified_by VARCHAR(32),

    CONSTRAINT pk_genre_videos_genre_id_video_id PRIMARY KEY (genre_id, video_id),
	CONSTRAINT fk_genre_videos_video_id FOREIGN KEY (video_id) REFERENCES videos (video_id)
);