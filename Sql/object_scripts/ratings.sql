IF (OBJECT_ID(N'dbo.ratings', N'U') IS NOT NULL)
BEGIN
	DROP TABLE dbo.ratings;
END

CREATE TABLE ratings (
    rating_id INT NOT NULL IDENTITY (1,1),
    source VARCHAR(32) NOT NULL,
    value DECIMAL(5,2) NOT NULL,
    added DATETIME,
    modified DATETIME,
    modified_by VARCHAR(32),

    CONSTRAINT pk_ratings_rating_id PRIMARY KEY (rating_id)
);