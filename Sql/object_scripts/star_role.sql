IF (OBJECT_ID(N'dbo.star_role', N'U') IS NOT NULL)
BEGIN
	DROP TABLE dbo.star_role;
END

CREATE TABLE dbo.star_role (
    role_id INT NOT NULL IDENTITY (1,1),
    role VARCHAR(16) NOT NULL UNIQUE,
    added DATETIME,
    modified DATETIME,
    modified_by VARCHAR(32),

    CONSTRAINT pk_star_role_role_id PRIMARY KEY (role_id)
);