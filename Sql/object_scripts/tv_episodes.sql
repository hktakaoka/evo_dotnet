IF (OBJECT_ID(N'dbo.tv_episodes', N'U') IS NOT NULL)
BEGIN
	DROP TABLE dbo.tv_episodes;
END

CREATE TABLE dbo.tv_episodes (
    tv_episode_id INT NOT NULL,
    video_id INT NOT NULL,
    season_number INT NOT NULL,
    episode_number INT NOT NULL,
    episode_name VARCHAR(64) NOT NULL,
    release_date DATETIME NOT NULL,
    resolution VARCHAR(16),
    codec VARCHAR(8),
    plot VARCHAR(8000),
    added DATETIME,
    modified DATETIME,
    modified_by VARCHAR(32),

    CONSTRAINT pk_tv_episodes_tv_episode_id PRIMARY KEY (tv_episode_id, video_id),
	CONSTRAINT fk_tv_episodes_video_id FOREIGN KEY (video_id) REFERENCES dbo.videos (video_id)
);