IF (OBJECT_ID(N'dbo.videos', N'U') IS NOT NULL)
BEGIN
	DROP TABLE dbo.videos;
END

CREATE TABLE dbo.videos (
    video_id INT NOT NULL,
	video_name VARCHAR(32) NOT NULL,
    title VARCHAR(64) NOT NULL,
    mpaa_rating VARCHAR(8),
    runtime DECIMAL(5,2),
    plot VARCHAR(8000),
    video_type VARCHAR(8) NOT NULL,
    release_date DATETIME NOT NULL,
    resolution VARCHAR(16),
    codec VARCHAR(8),
    added DATETIME,
    modified DATETIME,
    modified_by VARCHAR(32),

    CONSTRAINT pk_videos_video_id PRIMARY KEY (video_id)
);