IF (OBJECT_ID(N'dbo.rating_videos', N'U') IS NOT NULL)
BEGIN
	DROP TABLE dbo.rating_videos;
END

CREATE TABLE dbo.rating_videos (
    rating_id INT NOT NULL,
    video_id INT NOT NULL,
    added DATETIME,
    modified DATETIME,
    modified_by VARCHAR(32),

    CONSTRAINT pk_rating_videos_rating_id_video_id PRIMARY KEY (rating_id, video_id),
	CONSTRAINT fk_rating_videos_rating_id FOREIGN KEY (rating_id) REFERENCES dbo.ratings (rating_id),
	CONSTRAINT fk_rating_videos_video_id FOREIGN KEY (video_id) REFERENCES dbo.videos (video_id)
);