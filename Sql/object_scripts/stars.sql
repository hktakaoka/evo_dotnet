IF (OBJECT_ID(N'dbo.stars', N'U') IS NOT NULL)
BEGIN
	DROP TABLE dbo.stars;
END

CREATE TABLE dbo.stars (
    star_id INT NOT NULL IDENTITY (1,1),
    first_name VARCHAR(64) NOT NULL,
    middle_name VARCHAR(64),
    last_name VARCHAR(64) NOT NULL,
    suffix VARCHAR(64),
    role_id INT NOT NULL,
    added DATETIME,
    modified DATETIME,
    modified_by VARCHAR(32),

    CONSTRAINT pk_stars_star_id PRIMARY KEY (star_id),
	CONSTRAINT fk_star_role_role_id FOREIGN KEY (role_id) REFERENCES dbo.star_role (role_id),
    
	--We will have a hard time with dupe names if this is active.
	--CONSTRAINT unique_star_name UNIQUE(first_name, middle_name, last_name, suffix)
);