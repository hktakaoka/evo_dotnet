CREATE PROCEDURE add_star (
    @first_name VARCHAR(64),
    @middle_name VARCHAR(64) = NULL,
    @last_name VARCHAR(64),
    @suffix VARCHAR(64) = NULL,
    @role VARCHAR(16),
    @modified_by VARCHAR(32)
) AS
    BEGIN
        DECLARE @current_time DATETIME;
        SET @current_time = (
            SELECT getdate()
            );
        DECLARE @id INT;

        EXEC @id = dbo.star_exists @first_name, @last_name, @suffix, @middle_name;
        IF @id = 0
        BEGIN
            DECLARE @roleid INT;
            EXEC @roleid = dbo.add_star_role @role, @modified_by;

           INSERT INTO stars(first_name, middle_name, last_name, suffix, roleid, added, modified, modified_by)
            VALUES (@first_name, @middle_name, @last_name, @suffix, @roleid, @current_time, @current_time, @modified_by);

            EXEC @id = dbo.star_exists @first_name, @last_name, @suffix, @middle_name;
        end

        RETURN @id;
    end;