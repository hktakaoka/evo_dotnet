CREATE PROCEDURE drop_tables
AS
    BEGIN
        DROP TABLE IF EXISTS genre_videos;
        DROP TABLE IF EXISTS rating_videos;
        DROP TABLE IF EXISTS star_videos;
        DROP TABLE IF EXISTS tvepisodes;
        DROP TABLE IF EXISTS stars;
        DROP TABLE IF EXISTS star_role;
        DROP TABLE IF EXISTS genres;
        DROP TABLE IF EXISTS ratings;
        DROP TABLE IF EXISTS videos;
    end;

CREATE PROCEDURE create_tables
AS
    BEGIN
        CREATE TABLE genres (
            genreid INT NOT NULL IDENTITY (1,1),
            name VARCHAR(32) UNIQUE,
            added DATETIME,
            modified DATETIME,
            modified_by VARCHAR(32),

            CONSTRAINT pk_genres_genreid PRIMARY KEY (genreid)
        );

        CREATE TABLE ratings (
            ratingid INT NOT NULL IDENTITY (1,1),
            source VARCHAR(32) NOT NULL,
            value DECIMAL(5,2) NOT NULL,
            added DATETIME,
            modified DATETIME,
            modified_by VARCHAR(32),

            CONSTRAINT pk_ratings_ratingid PRIMARY KEY (ratingid),
            CONSTRAINT unique_ratings_source_value UNIQUE (source,value)
        );

        CREATE TABLE star_role (
            roleid INT NOT NULL IDENTITY (1,1),
            role VARCHAR(16) NOT NULL UNIQUE,
            added DATETIME,
            modified DATETIME,
            modified_by VARCHAR(32),

            CONSTRAINT pk_star_role_roleid PRIMARY KEY (roleid)
        );

        CREATE TABLE stars (
            starid INT NOT NULL IDENTITY (1,1),
            first_name VARCHAR(64) NOT NULL,
            middle_name VARCHAR(64),
            last_name VARCHAR(64) NOT NULL,
            suffix VARCHAR(64),
            roleid INT NOT NULL,
            added DATETIME,
            modified DATETIME,
            modified_by VARCHAR(32),

            CONSTRAINT pk_stars_starid PRIMARY KEY (starid),
            CONSTRAINT fk_star_role_roleid FOREIGN KEY (roleid)
                           REFERENCES star_role (roleid)
                           ON DELETE CASCADE
                           ON UPDATE CASCADE,
            CONSTRAINT unique_star_name UNIQUE(first_name,middle_name,last_name,suffix)
        );

        CREATE TABLE videos (
            videoid VARCHAR(32) NOT NULL,
            title VARCHAR(64) NOT NULL,
            mpaa_rating VARCHAR(8),
            runtime DECIMAL(5,2),
            plot VARCHAR(8000),
            video_type VARCHAR(8) NOT NULL,
            release_date DATETIME NOT NULL,
            resolution VARCHAR(16),
            codec VARCHAR(8),
            added DATETIME,
            modified DATETIME,
            modified_by VARCHAR(32),

            CONSTRAINT pk_videos_videoid PRIMARY KEY (videoid)
        );

        CREATE TABLE tvepisodes (
            tvepisodeid VARCHAR(32) NOT NULL,
            videoid VARCHAR(32) NOT NULL,
            season_number INT NOT NULL,
            episode_number INT NOT NULL,
            episode_name VARCHAR(64) NOT NULL,
            release_date DATETIME NOT NULL,
            resolution VARCHAR(16),
            codec VARCHAR(8),
            plot VARCHAR(8000),
            added DATETIME,
            modified DATETIME,
            modified_by VARCHAR(32),

            CONSTRAINT pk_tvepisodes_evepisodeid PRIMARY KEY (tvepisodeid,videoid),
            CONSTRAINT fk_tvepisodes_videoid FOREIGN KEY (videoid)
                                REFERENCES videos (videoid)
                                ON UPDATE CASCADE
                                ON DELETE CASCADE
        );

        CREATE TABLE genre_videos (
            genreid INT NOT NULL,
            videoid VARCHAR(32) NOT NULL,
            added DATETIME,
            modified DATETIME,
            modified_by VARCHAR(32),

            CONSTRAINT pk_genre_videos_genreid_videoid PRIMARY KEY (genreid,videoid),
            CONSTRAINT fk_genre_videos_genreid FOREIGN KEY (genreid)
                                  REFERENCES genres (genreid)
                                  ON DELETE CASCADE
                                  ON UPDATE CASCADE,
            CONSTRAINT fk_genre_videos_video_id FOREIGN KEY (videoid)
                                  REFERENCES videos (videoid)
                                  ON DELETE CASCADE
                                  ON UPDATE CASCADE
        );

        CREATE TABLE star_videos (
            starid INT NOT NULL,
            videoid VARCHAR(32) NOT NULL,
            added DATETIME,
            modified DATETIME,
            modified_by VARCHAR(32),

            CONSTRAINT pk_star_videos_starid_videoid PRIMARY KEY (starid,videoid),
            CONSTRAINT fk_star_videos_starid FOREIGN KEY (starid)
                                 REFERENCES stars (starid)
                                 ON UPDATE CASCADE
                                 ON DELETE CASCADE,
            CONSTRAINT fk_star_videos_videoid FOREIGN KEY (videoid)
                                 REFERENCES videos (videoid)
                                 ON UPDATE CASCADE
                                 ON DELETE CASCADE
        );

        CREATE TABLE rating_videos (
            ratingid INT NOT NULL,
            videoid VARCHAR(32) NOT NULL,
            added DATETIME,
            modified DATETIME,
            modified_by VARCHAR(32),

            CONSTRAINT pk_rating_videos_ratingid_videoid PRIMARY KEY (ratingid,videoid),
            CONSTRAINT fk_rating_videos_ratingid FOREIGN KEY (ratingid)
                                 REFERENCES ratings (ratingid)
                                 ON UPDATE CASCADE
                                 ON DELETE CASCADE,
            CONSTRAINT fk_rating_videos_videoid FOREIGN KEY (videoid)
                                 REFERENCES videos (videoid)
                                 ON UPDATE CASCADE
                                 ON DELETE CASCADE
        );

    end;

CREATE PROCEDURE reset_db
AS
    BEGIN
        EXEC dbo.drop_tables;
        EXEC dbo.create_tables;
    end;