CREATE PROCEDURE add_star_role (
    @role VARCHAR(16),
    @modified_by VARCHAR(32)
) AS
    BEGIN
        DECLARE @current_time DATETIME;
        SET @current_time = (
            SELECT getdate()
            );


        DECLARE @id INT;

        SET @id = (SELECT roleid
        FROM star_role
        WHERE role = @role);

        IF @id IS NULL
            BEGIN
                INSERT INTO star_role(role, added, modified, modified_by)
                    VALUES (@role, @current_time, @current_time, @modified_by);

                SET @id = (SELECT roleid
                FROM star_role
                WHERE role = @role);
            end;

        RETURN @id;
    end;