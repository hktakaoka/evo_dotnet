CREATE PROCEDURE add_video (
    @video_id VARCHAR(32),
    @title VARCHAR(64),
    @plot VARCHAR(8000),
    @video_type VARCHAR(8),
    @release_date DATETIME,
    @runtime DECIMAL(5,2) = NULL,
    @mpaa_rating VARCHAR(8) = NULL,
    @resolution VARCHAR(16) = NULL,
    @codec VARCHAR(8) = NULL,
    @modified_by VARCHAR(64)
)
AS
    BEGIN

        IF EXISTS (SELECT videoid FROM videos WHERE videoid = @video_id)
            BEGIN
                DECLARE @err_message VARCHAR(32);
                SET @err_message = @video_id + ' already exists.';
                RAISERROR (@err_message, 16, 1);
            end

        DECLARE @created_time DATETIME;

        SET @created_time = (
            SELECT getdate()
            );

        INSERT INTO videos(videoid, title, mpaa_rating, runtime, plot, video_type, release_date, resolution, codec, added, modified, modified_by)
            VALUES(@video_id, @title, @mpaa_rating, @runtime, @plot, @video_type, @release_date, @resolution, @codec, @created_time, @created_time, @modified_by);

    end;
