using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Evo.WebApi.Models.Requests;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.JoinerTables;

namespace Evo.WebApi.Tests.Extensions
{
    public static class DataModelExtensions
    {
        public static async Task<bool> IsEqual(this VideoRequest request, VideoDataModel result)
        {
            if (result == null && request != null || result != null && request == null) return false;

            var flatFieldsEqual = request.Codec == result.Codec
                                  && request.Plot == result.Plot
                                  && request.Resolution == result.Resolution
                                  && request.Runtime.Equals(result.Runtime)
                                  && request.Title == result.Title
                                  && request.Type == result.VideoType
                                  && request.ReleaseDate == result.ReleaseDate
                                  && request.VideoId == result.VideoDataModelId;

            if (!flatFieldsEqual) return false;

            var genresEqual = request.Genres.All(result.GenreVideos.IsEqual);
            var actorsEqual = request.Actors.All(result.StarVideos.IsEqual);
            var writersEqual = request.Writers.All(result.StarVideos.IsEqual);
            var directorsEqual = request.Directors.All(result.StarVideos.IsEqual);
            var producersEqual = request.Producers.All(result.StarVideos.IsEqual);
            var ratingsEqual = request.Ratings.All(result.RatingVideos.IsEqual);

            return genresEqual && actorsEqual && writersEqual && directorsEqual && producersEqual && ratingsEqual;
        }

        private static bool IsEqual(this IEnumerable<GenreVideo> @this, GenreRequest genre)
        {
            return @this.Any(a => a.GenreDataModel.Name == genre.Name);
        }

        private static bool IsEqual(this IEnumerable<StarVideo> @this, StarRequest star)
        {
            return @this.Any(a => a.StarDataModel.FirstName == star.FirstName
                                  && a.StarDataModel.MiddleName == star.MiddleName
                                  && a.StarDataModel.LastName == star.LastName);
        }

        private static bool IsEqual(this IEnumerable<RatingVideo> @this, RatingRequest rating)
        {
            return @this.Any(a => a.RatingDataModel.Source == rating.Source
                                  && a.RatingDataModel.RatingValue.Equals(rating.RatingValue));
        }
    }
}