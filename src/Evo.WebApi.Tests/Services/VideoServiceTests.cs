using System;
using System.Linq;
using System.Threading.Tasks;
using AutoBogus;
using AutoFixture;
using AutoFixture.AutoMoq;
using Evo.WebApi.Exceptions;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Repositories.Interfaces;
using Evo.WebApi.Services;
using Evo.WebApi.Tests.Extensions;
using Evo.WebApi.Tests.Helpers.Models.Mapper;
using Evo.WebApi.Tests.Helpers.Models.Requests;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Services
{
    [TestFixture]
    public class VideoServiceTests
    {
        [SetUp]
        public void Setup()
        {
            _fixture = new Fixture();
            _fixture.Customize(new AutoMoqCustomization());
            var mapper = CreateMapper.GetMapper();
            _fixture.Inject(mapper);
        }

        private Fixture _fixture;

        [Test]
        public async Task ShouldCallRepositoryToDeleteVideo()
        {
            var video = new VideoDataModel
            {
                VideoDataModelId = "tt1234431"
            };

            var videoRepository = _fixture.Freeze<Mock<IVideoRepository>>();
            videoRepository.Setup(s => s.GetById("tt1234431")).ReturnsAsync(video);
            videoRepository.Setup(s => s.Delete(It.IsAny<VideoDataModel>()))
                .Returns(Task.CompletedTask);
            var service = _fixture.Create<VideoService>();

            await service.DeleteById("tt1234431");

            videoRepository.Verify(v => v.Delete(video), Times.Once);
        }

        [Test]
        public async Task ShouldProperlyFormatDataModelFromRequest()
        {
            var request = FakeVideoRequests.CreateVideoRequest("tt1234");

            var dataModel = default(VideoDataModel);
            var videoRepository = _fixture.Freeze<Mock<IVideoRepository>>();

            videoRepository.Setup(s => s.AddOrUpdate(It.IsAny<VideoDataModel>()))
                .Callback<VideoDataModel>(dm => dataModel = dm)
                .Returns(Task.CompletedTask);

            videoRepository.Setup(s => s.GetById("tt1234"))
                .ReturnsAsync(default(VideoDataModel));

            var genreRepo = _fixture.Freeze<Mock<IGenreRepository>>();
            genreRepo.Setup(s => s.GetGenre(It.IsAny<string>()))
                .Returns(default(GenreDataModel));

            var starRepo = _fixture.Freeze<Mock<IStarRepository>>();
            starRepo.Setup(s => s.GetStar(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>()))
                .Returns(default(StarDataModel));

            var ratingRepo = _fixture.Freeze<Mock<IRatingRepository>>();
            ratingRepo.Setup(s => s.GetRating(It.IsAny<string>(), It.IsAny<double>()))
                .Returns(default(RatingDataModel));

            var service = _fixture.Create<VideoService>();

            await service.AddOrUpdate(request);

            Assert.That(await request.IsEqual(dataModel));
        }

        [Test]
        public void ShouldReturnAllVideos()
        {
            var title = "Donnie Darko";
            var videoRepository = _fixture.Freeze<Mock<IVideoRepository>>();

            videoRepository.Setup(s => s.GetAll())
                .Returns(new AutoFaker<VideoDataModel>()
                    .RuleFor(r => r.Title, r => title)
                    .Generate(50));
            var service = _fixture.Create<VideoService>();

            var videos = service.GetVideos();

            Assert.That(videos.Count(), Is.EqualTo(50));
            Assert.That(videos.All(a => a.Title == title));
        }

        [Test]
        public async Task ShouldReturnOneVideoWithCorrespondingId()
        {
            var id = "tt123456789";
            var videoRepository = _fixture.Freeze<Mock<IVideoRepository>>();

            videoRepository.Setup(s => s.GetById(id))
                .ReturnsAsync(new VideoDataModel {VideoDataModelId = id});

            var service = _fixture.Create<VideoService>();

            var video = await service.GetById(id);

            Assert.That(video, Is.Not.Null, "Service returned null video");
            Assert.That(video.VideoDataModelId, Is.EqualTo(id), "Service returned wrong video");
        }

        [Test]
        public void ShouldThrowExceptionIfCallingDeleteOnVideoThatDoesNotExist()
        {
            var id = "tt1234431";
            var videoRepository = _fixture.Freeze<Mock<IVideoRepository>>();

            videoRepository.Setup(s => s.GetById(id))
                .ReturnsAsync(default(VideoDataModel));

            var service = _fixture.Create<VideoService>();

            Func<Task> notFound = async () => await service.DeleteById(id);

            notFound.Should()
                .Throw<EvoNotFoundException>()
                .WithMessage($"Video with id `{id}` not found");
        }

        [Test]
        public void ShouldThrowExceptionWhenAddingExistingVideoAndUpdateIsFalse()
        {
            var request = FakeVideoRequests.CreateVideoRequest("tt12345678");
            var videoRepository = _fixture.Freeze<Mock<IVideoRepository>>();

            videoRepository.Setup(s => s.GetById("tt12345678"))
                .ReturnsAsync(new VideoDataModel());

            var service = _fixture.Create<VideoService>();
            Func<Task> action = async () => await service.AddOrUpdate(request);

            action.Should()
                .Throw<EvoBadRequestException>()
                .WithMessage("Resource tt12345678 already exists.");
        }

        [Test]
        public void ShouldThrowExceptionWhenVideoIsNotFound()
        {
            var videoRepository = _fixture.Freeze<Mock<IVideoRepository>>();

            videoRepository.Setup(s => s.GetById(string.Empty))
                .ReturnsAsync(default(VideoDataModel));
            var service = _fixture.Create<VideoService>();

            Func<Task> action = async () => await service.GetById(string.Empty);

            action.Should().Throw<EvoNotFoundException>();
        }
    }
}