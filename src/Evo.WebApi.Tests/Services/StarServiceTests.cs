using System.Collections.Generic;
using System.Linq;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.Enums;
using Evo.WebApi.Models.Videos.JoinerTables;
using Evo.WebApi.Repositories.Interfaces;
using Evo.WebApi.Services;
using Evo.WebApi.Tests.Helpers.Extensions;
using Evo.WebApi.Tests.Helpers.Models.Database;
using Moq;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Services
{
    [TestFixture]
    public class StarServiceTests
    {
        [TestCase(PersonType.Actor, TestName = "Should return all actors")]
        [TestCase(PersonType.Writer, TestName = "Should return all writers")]
        [TestCase(PersonType.Director, TestName = "Should return all directors")]
        [TestCase(PersonType.Producer, TestName = "Should return all producers")]
        public void ShouldReturnStarsMatchingFilteredType(PersonType star)
        {
            var starRepo = new Mock<IStarRepository>();
            var starService = new StarService(starRepo.Object);

            starRepo.Setup(s => s.GetStars(25))
                .Returns(new List<StarDataModel>
                {
                    Fakes.GetStar("John"),
                    Fakes.GetStar("Jane", role: PersonType.Writer),
                    Fakes.GetStar("John", role: PersonType.Director),
                    Fakes.GetStar("Jane", role: PersonType.Producer)
                });

            var stars = starService.GetStars(star);

            Assert.That(stars.All(a => a.Role == star));
            Assert.That(stars.Count, Is.EqualTo(1));
        }
        
        [Test]
        public void ShouldReturnAllVideosByStar()
        {
            var starRepo = new Mock<IStarRepository>();
            var starService = new StarService(starRepo.Object);
            
            var johnSmith = Fakes.GetStar("John");
            var janeSmith = Fakes.GetStar("Jane");
            
            var videos = new List<VideoDataModel>()
            {
                Fakes.GetVideo("t123", "Some title").AddJoinerTable(johnSmith, janeSmith),
                Fakes.GetVideo("t1234", "Some Other Title").AddJoinerTable(janeSmith)
            };

            var starVideos = videos.SelectMany(GetStarVideos);

            starRepo.Setup(s => s.GetStars(25))
                .Returns(new List<StarDataModel>
                {
                    Fakes.GetStar("John", videos: starVideos.Where(f => f.StarDataModel.FirstName == "John" && f.StarDataModel.LastName == "Smith")),
                    Fakes.GetStar("Jane", role: PersonType.Writer),
                    Fakes.GetStar("Johnathon", role: PersonType.Director),
                    Fakes.GetStar("Jane", role: PersonType.Producer)
                });
            
            var videosWithStar = starService.GetVideosWithStar("John", "Smith");
            
            Assert.That(videosWithStar.Count, Is.EqualTo(1));
            Assert.That(videosWithStar.First().VideoDataModelId, Is.EqualTo("t123"));
        }

        private IEnumerable<StarVideo> GetStarVideos(VideoDataModel videoDataModel)
        {
            foreach (var star in videoDataModel.StarVideos)
            {
                yield return new StarVideo
                {
                    VideoDataModel = videoDataModel,
                    VideoDataModelId = videoDataModel.VideoDataModelId,
                    StarDataModel = star.StarDataModel,
                    StarDataModelId = star.StarDataModelId
                };
            }
        }
    }
}