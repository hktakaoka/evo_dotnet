using System;
using System.Collections.Generic;
using System.Linq;
using AutoBogus;
using Evo.WebApi.Controllers;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.Enums;
using Evo.WebApi.Models.ViewModels;
using Evo.WebApi.Services.Interfaces;
using Evo.WebApi.Tests.Helpers.Models.Mapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Controllers
{
    [TestFixture]
    public class StarsControllerTests
    {
        [TestCase("Actor", TestName = "Should return actors")]
        [TestCase("Writer", TestName = "Should return Writers")]
        [TestCase("Director", TestName = "Should return Directors")]
        [TestCase("Producer", TestName = "Should return Producers")]
        public void ShouldReturnStars(string starType)
        {
            var service = new Mock<IStarService>();
            var personType = Enum.Parse<PersonType>(starType);
            service.Setup(s => s.GetStars(personType))
                .Returns(
                    new AutoFaker<StarDataModel>()
                        .RuleFor(r => r.Role, r => personType)
                        .Generate(50));

            var controller = new StarsController(service.Object, CreateMapper.GetMapper());
            var starsResult = controller.GetStars(starType) as OkObjectResult;
            var stars = starsResult.Value as IEnumerable<Star>;
            Assert.That(stars.Count(), Is.EqualTo(50));
            Assert.That(stars.All(a => a.Role == personType));
        }

        [Test]
        public void ShouldReturnBadRequestIfStarTypeIsUnknown()
        {
            var service = new Mock<IStarService>();
            var controller = new StarsController(service.Object, CreateMapper.GetMapper());
            var starsResult = controller.GetStars("unknown") as BadRequestObjectResult;
            Assert.That(starsResult, Is.Not.Null);
            Assert.That(starsResult.StatusCode, Is.EqualTo(StatusCodes.Status400BadRequest));
            Assert.That(starsResult.Value.GetType().GetProperty("Error").GetValue(starsResult.Value),
                Is.EqualTo("unknown is an unknown star type."));
        }

        [Test]
        public void ShouldReturnServerErrorIfGetStarsThrowsException()
        {
            var service = new Mock<IStarService>();
            service.Setup(s => s.GetStars(PersonType.Actor))
                .Throws(new Exception("Some exception occured."));
            var controller = new StarsController(service.Object, CreateMapper.GetMapper());
            var starsResult = controller.GetStars("Actor") as ObjectResult;
            Assert.That(starsResult, Is.Not.Null);
            Assert.That(starsResult.StatusCode, Is.EqualTo(StatusCodes.Status500InternalServerError));
            Assert.That(starsResult.Value.GetType().GetProperty("Error").GetValue(starsResult.Value),
                Is.EqualTo("Some exception occured."));
        }
    }
}