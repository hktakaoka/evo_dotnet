using System;
using System.Threading.Tasks;
using AutoBogus;
using Evo.WebApi.Controllers;
using Evo.WebApi.Exceptions;
using Evo.WebApi.Models.Requests;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.ViewModels;
using Evo.WebApi.Services.Interfaces;
using Evo.WebApi.Tests.Helpers.Models.Mapper;
using Evo.WebApi.Tests.Helpers.Models.Requests;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Controllers
{
    [TestFixture]
    public class VideosControllerTests
    {
        [SetUp]
        public void Setup()
        {
            _service = new Mock<IVideoService>();

            var mapper = CreateMapper.GetMapper();
            _controller = new VideosController(_service.Object, mapper);
        }

        private Mock<IVideoService> _service;
        private VideosController _controller;

        [Test]
        public async Task GetByIdShouldReturnOkWhenCompleteSuccessfully()
        {
            var id = "Some id";
            _service.Setup(s => s.GetById(It.IsAny<string>()))
                .ReturnsAsync(new AutoFaker<VideoDataModel>().RuleFor(r => r.VideoDataModelId, r => id).Generate());

            var video = await _controller.GetVideoWithId(id) as OkObjectResult;
            Assert.That(video, Is.Not.Null,
                $"Result was not null, the body contained: `{JsonConvert.SerializeObject((await _controller.GetVideoWithId(id) as ObjectResult).Value)}`");
            Assert.That(video.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
        }

        [Test]
        public async Task ShouldDeleteItemById()
        {
            _service.Setup(s => s.DeleteById(It.IsAny<string>()))
                .Returns(Task.CompletedTask);

            var result = await _controller.DeleteById("tt1234321") as NoContentResult;
            _service.Verify(v => v.DeleteById("tt1234321"), Times.Once);

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public async Task ShouldReturnBadRequestIfAddingAVideoThatAlreadyExists()
        {
            _service.Setup(s => s.AddOrUpdate(It.IsAny<VideoRequest>(), It.IsAny<bool>()))
                .ThrowsAsync(new EvoBadRequestException("The resource id already exists."));

            var response =
                await _controller.AddVideo(FakeVideoRequests.CreateVideoRequest("id")) as BadRequestObjectResult;

            response.Should().NotBe(null);
            (response.Value as BadRequest).Should().NotBe(null);

            var result = response.Value as BadRequest;
            result.Id.Should().BeEquivalentTo("id");
            result.Message.Should().BeEquivalentTo("The resource id already exists.");
        }

        [Test]
        public async Task ShouldReturnCreatedWhenSuccessfulCreation()
        {
            var request = FakeVideoRequests.CreateVideoRequest("tt1234");
            _service.Setup(s => s.AddOrUpdate(request, false))
                .ReturnsAsync(new VideoDataModel
                {
                    VideoDataModelId = "tt1234"
                });

            var result = await _controller.AddVideo(request) as CreatedResult;

            result.Should().NotBe(null);
            result.Location.Should().Be("/videos/tt1234");
            (result.Value as Video).VideoId.Should().Be("tt1234");
        }

        [Test]
        public void ShouldReturnInternalServerErrorWhenAnyExceptionThrown()
        {
            _service.Setup(s => s.GetVideos())
                .Throws(new Exception());

            var objectResult = _controller.GetVideos() as ObjectResult;
            var videoResult = objectResult.Value as ErrorResponse;
            Assert.That(videoResult, Is.Not.Null);
            Assert.That(objectResult.StatusCode, Is.EqualTo(StatusCodes.Status500InternalServerError));
            Assert.That(videoResult.Error, Is.Not.Null);
        }

        [Test]
        public async Task ShouldReturnInternalServerErrorWhenDeleteFails()
        {
            _service.Setup(s => s.DeleteById(It.IsAny<string>()))
                .Throws(new Exception("Delete failed."));

            var result = await _controller.DeleteById("tt1234321") as ObjectResult;
            _service.Verify(v => v.DeleteById("tt1234321"), Times.Once);

            var error = result.Value as ErrorResponse;

            Assert.That(result, Is.Not.Null);
            Assert.That(error.Error, Is.EqualTo("Delete failed."));
        }

        [Test]
        public async Task ShouldReturnInternalServerErrorWhenOtherUnhandledExceptionsOccur()
        {
            _service.Setup(s => s.GetById(It.IsAny<string>()))
                .ThrowsAsync(new Exception());

            var objectResult = await _controller.GetVideoWithId("hunter2") as ObjectResult;
            var videoResult = objectResult.Value as ErrorResponse;
            Assert.That(videoResult, Is.Not.Null);
            Assert.That(objectResult.StatusCode, Is.EqualTo(StatusCodes.Status500InternalServerError));
            Assert.That(videoResult.Error, Is.Not.Null);
        }

        [Test]
        public async Task ShouldReturnNotFoundWhenEntryDoesNotExist()
        {
            var id = "Some id";
            _service.Setup(s => s.GetById(It.IsAny<string>()))
                .ThrowsAsync(new EvoNotFoundException($"Video {id} does not exist."));

            var video = await _controller.GetVideoWithId(id) as NotFoundResult;
            Assert.That(video, Is.Not.Null);
            Assert.That(video.StatusCode, Is.EqualTo(StatusCodes.Status404NotFound));
        }

        [Test]
        public async Task ShouldReturnNotFoundWhenVideoToBeDeletedIsNotFound()
        {
            var id = "tt431243";
            _service.Setup(s => s.DeleteById(It.IsAny<string>()))
                .Throws(new EvoNotFoundException($"Video with id `{id}` not found"));

            Func<Task<IActionResult>> result = async () => await _controller.DeleteById(id);

            result.Should()
                .NotThrow<EvoNotFoundException>();

            var response = await result() as NotFoundResult;
            Assert.That(response, Is.Not.Null);
        }

        [Test]
        public void ShouldReturnOkResultWithAllVideos_WhenSuccess()
        {
            _service.Setup(s => s.GetVideos())
                .Returns(new AutoFaker<VideoDataModel>().Generate(50));

            var videoResult = _controller.GetVideos() as OkObjectResult;
            Assert.That(videoResult, Is.Not.Null,
                $"Result was not null, the body contained: `{JsonConvert.SerializeObject((_controller.GetVideos() as ObjectResult).Value)}`");
            Assert.That(videoResult.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
        }
    }
}