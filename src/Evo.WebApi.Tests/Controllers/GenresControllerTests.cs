using System;
using System.Collections.Generic;
using System.Linq;
using AutoBogus;
using Evo.WebApi.Controllers;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.ViewModels;
using Evo.WebApi.Repositories.Interfaces;
using Evo.WebApi.Tests.Helpers.Models.Mapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Controllers
{
    [TestFixture]
    public class GenresControllerTests
    {
        [Test]
        public void ShouldReturnGenres()
        {
            var service = new Mock<IGenreRepository>();
            service.Setup(s => s.GetGenres())
                .Returns(new AutoFaker<GenreDataModel>().Generate(50));
            
            var controller = new GenresController(service.Object, CreateMapper.GetMapper());

            var genreResult = controller.GetGenres() as OkObjectResult;
            var genres = genreResult.Value as IEnumerable<Genre>;
            
            Assert.That(genres, Is.Not.Null);
            Assert.That(genres.Count(), Is.EqualTo(50));
        }

        [Test]
        public void ShouldReturnServerErrorIfRepositoryFails()
        {
            var service = new Mock<IGenreRepository>();
            service.Setup(s => s.GetGenres())
                .Throws(new Exception("Some exception occured."));
            
            var controller = new GenresController(service.Object, CreateMapper.GetMapper());

            var genres = controller.GetGenres() as ObjectResult;
            
            Assert.That(genres, Is.Not.Null);
            Assert.That(
                genres.Value.GetType().GetProperty("Error").GetValue(genres.Value),
                Is.EqualTo("Some exception occured."));
            
        }
    }
}