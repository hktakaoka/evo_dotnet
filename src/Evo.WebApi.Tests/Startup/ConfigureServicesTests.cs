using System;
using System.Linq;
using Evo.WebApi.Controllers;
using Evo.WebApi.Models.Contexts;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Startup
{
    [TestFixture]
    public class ConfigureServicesTests
    {
        private IServiceProvider _provider;

        [OneTimeSetUp]
        public void OnetimeSetup()
        {
            var services = new ServiceCollection();
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            config["ASPNETCORE_ENVIRONMENT"] = "Test";
            var startup = new WebApi.Startup(config);
            _provider = startup.ConfigureServices(services);
        }
        
        [Test]
        public void ShouldRegisterDbContext()
        {
            
            Assert.That(
                _provider.GetService(typeof(VideoContext)),
                Is.Not.Null,
                "Video Context could not be resolved");
        }

        [TestCase(typeof(VideosController), TestName = "Should resolve videos controller")]
        [TestCase(typeof(StarsController), TestName = "Should resolve stars controller")]
        public void ShouldResolveControllers(Type controllerType)
        {
            var controllers = _provider.GetServices<IActionFilter>();
            
            Assert.That(controllers.Where(w => w.GetType() == controllerType),
                Is.Not.Null,
                $"Could not resolve {controllerType}");
        }

        [TestCase("Service", TestName = "Should resolve services")]
        [TestCase("Repository", TestName = "Should resolve repositories")]
        public void ShouldResolveTypes(string type)
        {
            var services = typeof(Program).Assembly
                .GetTypes()
                .Where(w => w.IsInterface)
                .Where(w => w.Name.Contains(type, StringComparison.CurrentCultureIgnoreCase));
            
            Assert.Multiple(() =>
            {
                foreach (var service in services)
                {
                    Assert.That(
                        _provider.GetService(service),
                        Is.Not.Null,
                        $"Could not resolve {service.Name}");    
                }
            });
        }
    }
}