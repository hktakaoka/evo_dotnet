using System.Linq;
using System.Threading.Tasks;
using AutoBogus;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.JoinerTables;
using Evo.WebApi.Repositories;
using Evo.WebApi.Tests.Helpers.Extensions;
using Evo.WebApi.Tests.Helpers.Models.Database;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Repositories
{
    [TestFixture]
    public class AddUpdateVideo
    {
        private const string VideoId = "tt123456789";
        private const string UpdateId = "tt123456789Update";

        private VideoContext _videoContext;
        private VideoRepository _videoRepository;
        private SqliteConnection _connection;
        private DbContextOptions<VideoContext> _options;

        [OneTimeSetUp]
        public void EstablishConnection()
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();
        }

        [OneTimeTearDown]
        public void DestroyConnection()
        {
            _connection.Close();
            _connection.Dispose();
        }

        [SetUp]
        public async Task Setup()
        {
            _options = new DbContextOptionsBuilder<VideoContext>()
                .UseSqlite(_connection)
                .Options;

            using (var context = new VideoContext(_options))
            {
                await context.Database.EnsureCreatedAsync();
                var video = Fakes.GetVideo(UpdateId, "Fight Club");
                context.Videos.Add(video);
                context.SaveChanges();
            }

            _videoContext = new VideoContext(_options);
            _videoRepository = new VideoRepository(_videoContext);
        }

        [TearDown]
        public void Teardown()
        {
            var options = new DbContextOptionsBuilder<VideoContext>()
                .UseSqlite(_connection)
                .Options;

            using (var context = new VideoContext(options))
            {
                context.Videos.RemoveRange(_videoContext.Videos);
                context.Genres.RemoveRange(_videoContext.Genres);
                context.Stars.RemoveRange(_videoContext.Stars);
                context.Ratings.RemoveRange(_videoContext.Ratings);
                context.TvEpisodes.RemoveRange(_videoContext.TvEpisodes);

                context.SaveChanges();
                context.Database.EnsureDeleted();
            }

            _videoContext.Dispose();

        }

        [Test]
        public async Task ShouldUpdateVideoWithNewModel()
        {
            var video = Fakes.GetVideo(UpdateId, "Not Fight Club");
            await _videoRepository.AddOrUpdate(video);

            Assert.That(_videoContext.Videos
                    .Single(
                        s => s.VideoDataModelId == UpdateId)
                    .Title,
                Is.EqualTo("Not Fight Club"));
        }

        [Test]
        public async Task ShouldCreateNewVideoIfOneDidNotExistToUpdate()
        {
            var video = Fakes.GetVideo("ttIDontExist", "Definitely Not Fight Club");
            await _videoRepository.AddOrUpdate(video);

            Assert.That(_videoContext.Videos
                    .Single(
                        s => s.VideoDataModelId == "ttIDontExist")
                    .Title,
                Is.EqualTo("Definitely Not Fight Club"));
        }

        [Test]
        public async Task ShouldLinkWithCreatedGenre()
        {
            var genre = Fakes.GetGenre("Horror", 100);
            var video = Fakes.GetVideo("ttIProbablyExist", "Not Hunter 2");
            video.AddJoinerTable(genre);

            await _videoRepository.AddOrUpdate(video);

            using (var context = new VideoContext(_options))
            {
                var finalVideo = context.Videos
                    .Include(i => i.GenreVideos)
                    .ThenInclude(ti => ti.GenreDataModel)
                    .First(f => f.VideoDataModelId == "ttIProbablyExist");

                finalVideo.GenreVideos
                    .Single(s => s.GenreDataModelId == 100)
                    .GenreDataModel
                    .Name
                    .Should()
                    .BeEquivalentTo("Horror");

            }
        }

        [Test]
        public async Task ShouldLinkWithCreatedRating()
        {
            var genre = Fakes.GetRating("Horror", 100);
            var video = Fakes.GetVideo("ttIProbablyExist", "Not Hunter 2");
            video.AddJoinerTable(genre);

            await _videoRepository.AddOrUpdate(video);

            using (var context = new VideoContext(_options))
            {
                var finalVideo = context.Videos
                    .Include(i => i.RatingVideos)
                    .ThenInclude(ti => ti.RatingDataModel)
                    .First(f => f.VideoDataModelId == "ttIProbablyExist");

                finalVideo.RatingVideos
                    .Single(s => s.RatingDataModelId == 100)
                    .RatingDataModel
                    .Source
                    .Should()
                    .BeEquivalentTo("Horror");
            }
        }
        
        [Test]
        public async Task ShouldLinkWithCreatedStar()
        {
            var genre = Fakes.GetStar("Horror", 100);
            var video = Fakes.GetVideo("ttIProbablyExist", "Not Hunter 2");
            video.AddJoinerTable(genre);

            await _videoRepository.AddOrUpdate(video);

            using (var context = new VideoContext(_options))
            {
                var finalVideo = context.Videos
                    .Include(i => i.StarVideos)
                    .ThenInclude(ti => ti.StarDataModel)
                    .First(f => f.VideoDataModelId == "ttIProbablyExist");

                finalVideo.StarVideos
                    .Single(s => s.StarDataModelId == 100)
                    .StarDataModel
                    .FirstName
                    .Should()
                    .BeEquivalentTo("Horror");
            }
        }
    }
}