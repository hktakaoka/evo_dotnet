using System;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.Enums;
using Evo.WebApi.Repositories;
using Evo.WebApi.Tests.Helpers.Models.Database;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Repositories
{
    [TestFixture]
    public class GetStars
    {
        private SqliteConnection _connection;
        private DbContextOptions<VideoContext> _options;

        [OneTimeSetUp]
        public void EstablishConnection()
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();
        }

        [OneTimeTearDown]
        public void CloseConnection()
        {
            _connection.Close();
            _connection.Dispose();
        }

        [SetUp]
        public void SetupDatabase()
        {
            _options = new DbContextOptionsBuilder<VideoContext>()
                .UseSqlite(_connection)
                .Options;
            
            using (var context = new VideoContext(_options))
            {
                context.Database.EnsureCreated();
                
                context.Add(Fakes.GetStar("Jane"));
                context.Add(Fakes.GetStar("John"));
                context.Add(Fakes.GetStar("Jon"));
                
                context.SaveChanges();
            }
        }

        [TearDown]
        public void TearDownContext()
        {
            using (var context = new VideoContext(_options))
            {
                context.Stars.RemoveRange(context.Stars);
                context.SaveChanges();
            }
            
        }

        [Test]
        public void ShouldGetAllStars()
        {
            using (var context = new VideoContext(_options))
            {
                var repository = new StarRepository(context);
                var stars = repository.GetStars();
                Assert.That(
                    stars.Count(),
                    Is.EqualTo(3),
                    "The wrong number of stars have been returned");
            }
        }

        [Test]
        public void ShouldReturnAllStarsOrderedByLastNameAlphabetically()
        {
            using (var context = new VideoContext(_options))
            {
                context.Stars.Add(Fakes.GetStar("Hello", lastName: "Window"));
                context.SaveChanges();
            }
            
            using (var context = new VideoContext(_options))
            {
                var repository = new StarRepository(context);
                var stars = repository.GetStars();

                Assert.That(
                    stars.Last().LastName,
                    Is.EqualTo("Window"),
                    $"The last item should be Window, but was {stars.Last().LastName}");
            }
            
        }

        [Test]
        public void ShouldReturnMaximumOf25StarsWithoutSpecifyingCountNumber()
        {
            using (var context = new VideoContext(_options))
            {
                foreach (var _ in Enumerable.Range(1, 25))
                {
                    var faker = new Faker();
                    context.Stars.Add(
                        Fakes.GetStar(faker.Name.FirstName(), lastName: faker.Name.LastName()));
                }

                context.SaveChanges();
            }
            using (var context = new VideoContext(_options))
            {
                    var repository = new StarRepository(context);
                    var stars = repository.GetStars();

                    Assert.That(
                        stars.Count(),
                        Is.EqualTo(25),
                        $"Only 25 items should be returned without specifying count, {stars.Count()} was returned");
            }
            
        }

        [Test]
        public void ShouldReturnMaximumNumberOfStarsEntered()
        {
            using (var context = new VideoContext(_options))
            {
                foreach (var _ in Enumerable.Range(1, 25))
                {
                    var faker = new Faker();
                    context.Stars.Add(
                        Fakes.GetStar(faker.Name.FirstName(), lastName: faker.Name.LastName()));
                }

                context.SaveChanges();
            }
            
            using (var context = new VideoContext(_options)) 
            {
                var repository = new StarRepository(context);
                    var stars = repository.GetStars(30);
                    
                    Assert.That(
                        stars.Count(),
                        Is.LessThanOrEqualTo(30),
                        $"Only 25 items should be returned without specifying count, {stars.Count()} was returned");
            }
        }

        [Test]
        public void ShouldCreateNewStar()
        {
            var star = new StarDataModel
            {
                Role = PersonType.Actor,
                FirstName = "Jonathon",
                LastName = "Smith",
                MiddleName = "F."
            };

            using (var context = new VideoContext(_options))
            {
                var repo = new StarRepository(context);
                repo.AddStar(star).GetAwaiter().GetResult();
            }

            using (var context = new VideoContext(_options))
            {
                var jonathon = context.Stars.Single(
                    s => s.FirstName == "Jonathon" 
                         && s.MiddleName == "F."
                         && s.LastName == "Smith");

                jonathon.Should().NotBe(null);
            }
        }

        [Test]
        public void ShouldNotDoAnythingIfStarAlreadyExists()
        {
            StarDataModel star;

            using (var context = new VideoContext(_options))
            {
                star = context.Stars.FirstOrDefault();
            }

            using (var context = new VideoContext(_options))
            {
                var repo = new StarRepository(context);
                Func<Task> action = async () => await repo.AddStar(star);
                
                
                action.Should().NotThrow();
            }
        }
    }
}