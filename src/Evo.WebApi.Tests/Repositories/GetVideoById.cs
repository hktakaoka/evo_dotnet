using System.Linq;
using System.Threading.Tasks;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Repositories;
using Evo.WebApi.Tests.Helpers.Extensions;
using Evo.WebApi.Tests.Helpers.Models.Database;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Repositories
{
    [TestFixture]
    public class GetVideoById
    {
        private const string _videoId = "tt123456789";
        private VideoContext _videoContext;
        private SqliteConnection _connection;
        
        [SetUp]
        public void Setup()
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();
            var options = new DbContextOptionsBuilder<VideoContext>()
                .UseSqlite(_connection)
                .Options;

            using (var context = new VideoContext(options))
            {
                context.Database.EnsureCreated();
                
                var otherVideo = Fakes.GetVideo("who cares what it is");
                
                foreach (var count in Enumerable.Range(0, 4))
                {
                    var video = Fakes.GetVideo(count == 0 ? _videoId : $"{_videoId}{count}");

                    video.AddJoinerTable(Fakes.GetRating("Metacritic", id: 1));
                    video.AddJoinerTable(Fakes.GetGenre("Horror", id: 1));
                    video.AddJoinerTable(Fakes.GetStar("Nancy", id: 1));

                    if (count == 0)
                    {
                        foreach (var innerCount in Enumerable.Range(0, 3))
                        {
                            video.TvEpisodes.Add(Fakes.GetEpisode($"tt987654321{innerCount}", video));
                        }
                    }
                    
                    
                    context.Videos.Add(video);
                }
                
                context.Ratings.Add(Fakes.GetRating("Rotten Tomatoes", id: 2));
                otherVideo.TvEpisodes.Add(Fakes.GetEpisode($"tt1234fdas456", otherVideo));
                context.Videos.Add(otherVideo);
                
                context.SaveChanges();
            }

            
            _videoContext = new VideoContext(options);
        }

        [TearDown]
        public void Teardown()
        {
            using (_videoContext)
            {
                _videoContext.Videos.RemoveRange(_videoContext.Videos);
                _videoContext.Genres.RemoveRange(_videoContext.Genres);
                _videoContext.Stars.RemoveRange(_videoContext.Stars);
                _videoContext.Ratings.RemoveRange(_videoContext.Ratings);
                _videoContext.TvEpisodes.RemoveRange(_videoContext.TvEpisodes);
                _videoContext.SaveChanges();
            }
            
            _connection.Close();
            _connection.Dispose();
        }
        
        [Test]
        public async Task ShouldGetAVideoWithCorrespondingImdbId()
        {
            var videoService = new VideoRepository(_videoContext);
            
            var result = await videoService.GetById(_videoId);
            
            Assert.That(result.VideoDataModelId, Is.EqualTo(_videoId));
        }

        [Test]
        public async Task ShouldShowWhatGenresForTheMovie()
        {
            var videoService = new VideoRepository(_videoContext);
            
            var video = await videoService.GetById(_videoId);
            
            Assert.That(video.VideoDataModelId, Is.EqualTo(_videoId));
            Assert.That(_videoContext.Genres.SingleOrDefault(), Is.Not.Null);
            Assert.That(video.GenreVideos, Is.Not.Null);
            Assert.That(video.GenreVideos.Count, Is.GreaterThan(0));
            Assert.That(video.GenreVideos.Single().GenreDataModel.Name, Is.EqualTo("Horror"));
        }

        [Test]
        public async Task ShouldShowWhatStarsInTheMovie()
        {
            var videoService = new VideoRepository(_videoContext);
            var video = await videoService.GetById(_videoId);
            
            Assert.That(video.VideoDataModelId, Is.EqualTo(_videoId));
            Assert.That(_videoContext.Stars.SingleOrDefault(), Is.Not.Null);
            Assert.That(video.StarVideos, Is.Not.Null);
            Assert.That(video.StarVideos.Count, Is.EqualTo(1));
            Assert.That(video.StarVideos.Single().StarDataModel.FirstName, Is.EqualTo("Nancy"));
        }

        [Test]
        public async Task ShouldShowTheRatingsAssociated()
        {
            var videoService = new VideoRepository(_videoContext);
            var video = await videoService.GetById(_videoId);
            
            Assert.That(video.VideoDataModelId, Is.EqualTo(_videoId));
            Assert.That(_videoContext.Ratings.Count(), Is.EqualTo(2));
            Assert.That(video.RatingVideos, Is.Not.Null);
            Assert.That(video.RatingVideos.Count, Is.EqualTo(1));
            Assert.That(video.RatingVideos.Single().RatingDataModel.Source, Is.EqualTo("Metacritic"));
        }

        [Test]
        public async Task ShouldShowTheAssociatedTvEpisodes()
        {
            var videoService = new VideoRepository(_videoContext);
            var video = await videoService.GetById(_videoId);
            
            Assert.That(video.VideoDataModelId, Is.EqualTo(_videoId));
            Assert.That(_videoContext.TvEpisodes.Count(), Is.EqualTo(4));
            Assert.That(video.TvEpisodes, Is.Not.Null);
            Assert.That(video.TvEpisodes.Count, Is.EqualTo(3));
            Assert.That(video.TvEpisodes.First().TvEpisodeDataModelId, Is.EqualTo("tt9876543210"));
        }

        [Test]
        public async Task ShouldReturnNull_WhenIdDoesntExist()
        {
            var videoService = new VideoRepository(_videoContext);
            var video = await videoService.GetById("DoesNotExist");
            
            Assert.That(video, Is.Null, "Video does exist");
        }
        
       
    }
}
