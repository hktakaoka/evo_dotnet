using System;
using System.Linq;
using System.Threading.Tasks;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Repositories;
using Evo.WebApi.Tests.Helpers.Models.Database;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Repositories
{
    [TestFixture]
    public class GetANumberOfVideos
    {
        private const string VideoId = "tt123456789";
        
        private VideoContext _videoContext;
        private VideoRepository _videoRepository;
        private SqliteConnection _connection;

        [OneTimeSetUp]
        public void EstablishConnection()
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();
        }

        [OneTimeTearDown]
        public void DestroyConnection()
        {
            _connection.Close();
            _connection.Dispose();
        }

        [SetUp]
        public async Task Setup()
        {
            var options = new DbContextOptionsBuilder<VideoContext>()
                .UseSqlite(_connection)
                .Options;

            using (var context = new VideoContext(options))
            {
                await context.Database.EnsureCreatedAsync();

                foreach (var count in Enumerable.Range(1, 5))
                {
                    var video = Fakes.GetVideo($"{VideoId}{count}");
                    context.Videos.Add(video);
                }
                
                context.SaveChanges();
            }
            
            _videoContext = new VideoContext(options);
            _videoRepository = new VideoRepository(_videoContext);
        }

        [TearDown]
        public void Teardown()
        {
            _videoContext.Videos.RemoveRange(_videoContext.Videos);
            _videoContext.SaveChanges();
            _videoContext.Database.EnsureDeleted();
            _videoContext.Dispose();
        }
        
        [Test]
        public void ShouldReturnAllVideosOrderedById()
        {
            var videos = _videoRepository.GetAll();
            
            Assert.That(videos.First().VideoDataModelId, Is.EqualTo($"{VideoId}1"));
            Assert.That(videos.Last().VideoDataModelId, Is.EqualTo($"{VideoId}5"));
        }
    }
}