using System.Linq;
using System.Threading.Tasks;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Repositories;
using Evo.WebApi.Tests.Helpers.Models.Database;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Repositories
{
    [TestFixture]
    public class AddRatings
    {
        private SqliteConnection _connection;
        private DbContextOptions<VideoContext> _options;

        [OneTimeSetUp]
        public void EstablishConnection()
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();
        }

        [OneTimeTearDown]
        public void CloseConnection()
        {
            _connection.Close();
            _connection.Dispose();
        }

        [SetUp]
        public void SetupDatabase()
        {
            _options = new DbContextOptionsBuilder<VideoContext>()
                .UseSqlite(_connection)
                .Options;
            
            using (var context = new VideoContext(_options))
            {
                context.Database.EnsureCreated();
                var rating = Fakes.GetRating("Rotten Tomato", 9);
                context.Ratings.Add(rating);
                context.SaveChanges();
            }
        }

        [TearDown]
        public void TearDownContext()
        {
            using (var context = new VideoContext(_options))
            {
                context.Ratings.RemoveRange(context.Ratings);
                context.SaveChanges();
            }
            
        }

        [Test]
        public void ShouldAddRating()
        {
            var rating = Fakes.GetRating("Metacritic");

            using (var context = new VideoContext(_options))
            {
                var repo = new RatingRepository(context);
                repo.AddRating(rating).GetAwaiter().GetResult();
            }
            
            using (var context = new VideoContext(_options))
            {
                context.Ratings.Single(s => s.Source == "Metacritic").Should().BeEquivalentTo(rating);
            }
        }
        
        [Test]
        public void ShouldNotDoAnythingIfRatingAlreadyExists()
        {
            var rating = Fakes.GetRating("Rotten Tomato", 9);

            using (var context = new VideoContext(_options))
            {
                var repo = new RatingRepository(context);
                repo.AddRating(rating).GetAwaiter().GetResult();
            }
            
            using (var context = new VideoContext(_options))
            {
                context.Ratings.Single(s => s.Source == "Rotten Tomato").Should().BeEquivalentTo(rating);
                context.Ratings.First().RatingDataModelId.Should().Be(9);
            }
        }
    }
}