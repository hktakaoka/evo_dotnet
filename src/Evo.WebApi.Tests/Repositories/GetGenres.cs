using System;
using System.Linq;
using System.Threading.Tasks;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Repositories;
using Evo.WebApi.Tests.Helpers.Models.Database;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Evo.WebApi.Tests.Repositories
{
    [TestFixture]
    public class GetGenres
    {
        private SqliteConnection _connection;
        private VideoContext _context;
        private GenreRepository _repository;
        private DbContextOptions<VideoContext> _options;

        [OneTimeSetUp]
        public void EstablishConnection()
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();
        }

        [OneTimeTearDown]
        public void CloseConnection()
        {
            _connection.Close();
            _connection.Dispose();
        }

        [SetUp]
        public void SetupDatabase()
        {
            _options = new DbContextOptionsBuilder<VideoContext>()
                .UseSqlite(_connection)
                .Options;
            
            using (var context = new VideoContext(_options))
            {
                context.Database.EnsureCreated();
                
                context.Add(Fakes.GetGenre("Horror", 1));
                context.Add(Fakes.GetGenre("Comedy"));
                context.Add(Fakes.GetGenre("Action"));
                
                context.SaveChanges();
            }
            _context = new VideoContext(_options);
            _repository = new GenreRepository(_context);
        }
        
        [TearDown]
        public void TearDownContext()
        {
            using (var context = new VideoContext(_options))
            {
                context.Genres.RemoveRange(_context.Genres);
                context.SaveChanges();
                context.Database.EnsureDeleted();
                _context.Dispose();
            }
        }

        [Test]
        public void ShouldReturnAllGenres()
        {
            var genres = _repository.GetGenres();
            Assert.That(genres.Count(), Is.EqualTo(3));
        }

        [Test]
        public void ShouldAddGenreIfItDoesNotExist()
        {
            var genreToAdd = new GenreDataModel
            {
                Name = "Horror"
            };
            _repository.AddGenre(genreToAdd).GetAwaiter().GetResult();

            var adventure = _context.Genres.Single(s => s.Name == "Horror");
            
            adventure.Should().NotBe(null);
            adventure.GenreDataModelId.Should().Be(1);
        }

        [Test]
        public void ShouldNotDoAnythingIfGenreAlreadyExists()
        {
            var genreToAdd = new GenreDataModel
            {
                Name = "Horror"
            };
            Func<Task> action = async () => await _repository.AddGenre(genreToAdd);
            
            action.Should().NotThrow();
        }
    }
}