using System.Collections.Generic;
using System.Linq;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.Enums;
using Evo.WebApi.Repositories.Interfaces;
using Evo.WebApi.Services.Interfaces;

namespace Evo.WebApi.Services
{
    public class StarService : IStarService
    {
        private readonly IStarRepository _starRepository;

        public StarService(IStarRepository starRepository)
        {
            _starRepository = starRepository;
        }

        public IEnumerable<StarDataModel> GetStars(PersonType star)
        {
            var stars = _starRepository.GetStars(); 
            return stars.Where(w => w.Role == star);
        }

        public IEnumerable<VideoDataModel> GetVideosWithStar(string firstName, string lastName)
        {
            return _starRepository
                .GetStars()
                .SelectMany(
                    s => s.StarVideos
                        .Select(s1 => s1.VideoDataModel)
                        .Where(w => w.StarVideos
                            .Any(a => a.StarDataModel.FirstName == firstName && a.StarDataModel.LastName == lastName)));
        }
    }
}