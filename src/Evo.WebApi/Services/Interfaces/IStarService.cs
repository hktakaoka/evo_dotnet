using System.Collections.Generic;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.Enums;

namespace Evo.WebApi.Services.Interfaces
{
    public interface IStarService
    {
        IEnumerable<StarDataModel> GetStars(PersonType star);
        IEnumerable<VideoDataModel> GetVideosWithStar(
            string firstName,
            string lastName);
    }
}