using System.Collections.Generic;
using System.Threading.Tasks;
using Evo.WebApi.Models.Requests;
using Evo.WebApi.Models.Videos;

namespace Evo.WebApi.Services.Interfaces
{
    public interface IVideoService
    {
        Task<VideoDataModel> GetById(string id);
        IEnumerable<VideoDataModel> GetVideos();
        Task DeleteById(string id);
        Task<VideoDataModel> AddOrUpdate(VideoRequest request, bool update = false);
    }
}