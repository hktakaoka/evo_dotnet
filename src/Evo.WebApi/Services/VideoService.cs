using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Evo.WebApi.Exceptions;
using Evo.WebApi.Models.Requests;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.JoinerTables;
using Evo.WebApi.Repositories.Interfaces;
using Evo.WebApi.Services.Interfaces;

namespace Evo.WebApi.Services
{
    public class VideoService : IVideoService
    {
        private readonly IGenreRepository _genreRepository;
        private readonly IMapper _mapper;
        private readonly IRatingRepository _ratingRepository;
        private readonly IStarRepository _starRepository;
        private readonly IVideoRepository _videoRepository;

        public VideoService(
            IMapper mapper,
            IVideoRepository videoRepository,
            IGenreRepository genreRepository,
            IStarRepository starRepository,
            IRatingRepository ratingRepository)
        {
            _videoRepository = videoRepository;
            _genreRepository = genreRepository;
            _starRepository = starRepository;
            _ratingRepository = ratingRepository;
            _mapper = mapper;
        }

        public async Task<VideoDataModel> GetById(string id)
        {
            var video = await _videoRepository.GetById(id);

            if (video == null) throw new EvoNotFoundException($"Video with id `{id}` not found");

            return video;
        }

        public IEnumerable<VideoDataModel> GetVideos()
        {
            var videos = _videoRepository.GetAll();

            return videos;
        }

        public async Task DeleteById(string id)
        {
            var video = await _videoRepository.GetById(id);
            if (video == null) throw new EvoNotFoundException($"Video with id `{id}` not found");
            await _videoRepository.Delete(video);
        }

        public async Task<VideoDataModel> AddOrUpdate(VideoRequest request, bool update = false)
        {
            if (update == false)
            {
                var video = await _videoRepository.GetById(request.VideoId);
                if (video != null) throw new EvoBadRequestException($"Resource {request.VideoId} already exists.");
            }

            var dataModel = _mapper.Map<VideoDataModel>(request);
            dataModel.GenreVideos = dataModel.GenreVideos.Concat(
                request.Genres.Select(s => GetGenre(s, dataModel))).ToList();
            dataModel.StarVideos = dataModel.StarVideos.Concat(
                request.Actors.Select(s => GetStars(s, dataModel))).ToList();
            dataModel.StarVideos = dataModel.StarVideos.Concat(
                request.Producers.Select(s => GetStars(s, dataModel))).ToList();
            dataModel.StarVideos = dataModel.StarVideos.Concat(
                request.Writers.Select(s => GetStars(s, dataModel))).ToList();
            dataModel.StarVideos = dataModel.StarVideos.Concat(
                request.Directors.Select(s => GetStars(s, dataModel))).ToList();

            dataModel.RatingVideos = dataModel.RatingVideos.Concat(
                request.Ratings.Select(s => GetRating(s, dataModel))).ToList();

            await _videoRepository.AddOrUpdate(dataModel);

            return dataModel;
        }

        private GenreVideo GetGenre(GenreRequest s, VideoDataModel dataModel)
        {
            var genre = _genreRepository.GetGenre(s.Name);

            return genre == null
                ? new GenreVideo
                {
                    GenreDataModel = _mapper.Map<GenreDataModel>(s),
                    VideoDataModel = dataModel,
                    VideoDataModelId = dataModel.VideoDataModelId
                }
                : new GenreVideo
                {
                    GenreDataModel = null,
                    GenreDataModelId = genre.GenreDataModelId,
                    VideoDataModel = dataModel,
                    VideoDataModelId = dataModel.VideoDataModelId
                };
        }

        private RatingVideo GetRating(RatingRequest s, VideoDataModel dataModel)
        {
            var genre = _ratingRepository.GetRating(s.Source, s.RatingValue);

            return genre == null
                ? new RatingVideo
                {
                    RatingDataModel = _mapper.Map<RatingDataModel>(s),
                    VideoDataModel = dataModel,
                    VideoDataModelId = dataModel.VideoDataModelId
                }
                : new RatingVideo
                {
                    RatingDataModel = null,
                    RatingDataModelId = genre.RatingDataModelId,
                    VideoDataModel = dataModel,
                    VideoDataModelId = dataModel.VideoDataModelId
                };
        }

        private StarVideo GetStars(StarRequest s, VideoDataModel dataModel)
        {
            var star = _starRepository.GetStar(s.FirstName, s.MiddleName, s.LastName);
            return star == null
                ? new StarVideo
                {
                    StarDataModel = _mapper.Map<StarDataModel>(s),
                    VideoDataModel = dataModel,
                    VideoDataModelId = dataModel.VideoDataModelId
                }
                : new StarVideo
                {
                    StarDataModel = null,
                    StarDataModelId = star.StarDataModelId,
                    VideoDataModel = dataModel,
                    VideoDataModelId = dataModel.VideoDataModelId
                };
        }
    }
}