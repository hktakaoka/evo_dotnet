namespace Evo.WebApi.Models.ViewModels
{
    public class BadRequest
    {
        public string Id { get; set; }
        public string Message { get; set; }
    }
}