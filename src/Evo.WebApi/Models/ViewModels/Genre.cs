namespace Evo.WebApi.Models.ViewModels
{
    public class Genre
    {
        public string Name { get; set; }
    }
}