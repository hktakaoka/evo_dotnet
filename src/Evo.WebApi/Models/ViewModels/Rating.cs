namespace Evo.WebApi.Models.ViewModels
{
    public class Rating
    {
        public string Source { get; set; }
        public double RatingValue { get; set; }
    }
}