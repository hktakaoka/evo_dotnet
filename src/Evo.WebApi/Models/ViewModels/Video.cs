using System;
using System.Collections.Generic;
using Evo.WebApi.Models.Videos.Enums;

namespace Evo.WebApi.Models.ViewModels
{
    public class Video
    {
        public string VideoId { get; set; }
        public DateTime Added { get; set; }
        public string Title { get; set; }
        public string MpaaRating { get; set; }
        public double Runtime { get; set; }
        public string Plot { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Resolution { get; set; }
        public string Codec { get; set; }
        public VideoType VideoType { get; set; }
        
        public IEnumerable<Genre> Genres { get; set; }
        public IEnumerable<Rating> Ratings { get; set; }
        public IEnumerable<Star> Stars { get; set; }
        public IEnumerable<TvEpisode> TvEpisodes { get; set; }
    }
}