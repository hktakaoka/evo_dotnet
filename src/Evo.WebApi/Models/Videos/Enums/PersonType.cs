namespace Evo.WebApi.Models.Videos.Enums
{
    public enum PersonType
    {
        Actor,
        Director,
        Writer,
        Producer
    }
}