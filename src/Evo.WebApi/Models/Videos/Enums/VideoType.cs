namespace Evo.WebApi.Models.Videos.Enums
{
    public enum VideoType
    {
        Video,
        TvShow
    }
}