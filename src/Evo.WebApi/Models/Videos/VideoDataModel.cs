using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Evo.WebApi.Models.Videos.Enums;
using Evo.WebApi.Models.Videos.JoinerTables;

namespace Evo.WebApi.Models.Videos
{
    public class VideoDataModel
    {
        public string VideoDataModelId { get; set; }
        [Required]
        public string Title { get; set; }
        public string MpaaRating { get; set; }
        public double Runtime { get; set; }
        [Required]
        public string Plot { get; set; }
        [Required]
        public VideoType VideoType { get; set; }
        [Required]
        public DateTime ReleaseDate { get; set; }
        public string Resolution { get; set; }
        public string Codec { get; set; }
        public DateTime Added { get; set; }
        public DateTime LastModified { get; set; }
        
        public ICollection<GenreVideo> GenreVideos { get; set; }
        public ICollection<RatingVideo> RatingVideos { get; set; }
        public ICollection<StarVideo> StarVideos { get; set; }
        public ICollection<TvEpisodeDataModel> TvEpisodes { get; set; }

        public VideoDataModel()
        {
            GenreVideos = new List<GenreVideo>();
            RatingVideos = new List<RatingVideo>();
            StarVideos = new List<StarVideo>();
            TvEpisodes = new List<TvEpisodeDataModel>();
        }
    }
}