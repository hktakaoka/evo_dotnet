using System;
using System.ComponentModel.DataAnnotations;

namespace Evo.WebApi.Models.Videos
{
    public class TvEpisodeDataModel
    {
        public string TvEpisodeDataModelId { get; set; }
        
        public string VideoDataModelId { get; set; }
        public VideoDataModel VideoDataModel { get; set; }
        
        [Required]
        public int SeasonNumber { get; set; }
        [Required]
        public int EpisodeNumber { get; set; }
        [Required]
        public string EpisodeName { get; set; }
        [Required]
        public DateTime ReleaseDate { get; set; }
        public string Resolution { get; set; }
        public string Codec { get; set; }
        [Required]
        public string Plot { get; set; }
    }
}