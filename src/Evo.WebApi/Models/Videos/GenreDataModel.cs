using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Evo.WebApi.Models.Videos.JoinerTables;

namespace Evo.WebApi.Models.Videos
{
    public class GenreDataModel
    {
        public int GenreDataModelId { get; set; }
        [Required]
        public string Name { get; set; }
        
        public ICollection<GenreVideo> GenreVideos { get; set; }


        public GenreDataModel()
        {
            GenreVideos = new List<GenreVideo>();
        }
    }
}