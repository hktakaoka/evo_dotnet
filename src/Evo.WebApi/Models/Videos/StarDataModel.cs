using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Evo.WebApi.Models.Videos.Enums;
using Evo.WebApi.Models.Videos.JoinerTables;

namespace Evo.WebApi.Models.Videos
{
    public class StarDataModel
    {
        public int StarDataModelId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        [Required]
        public PersonType Role { get; set; }
        
        public ICollection<StarVideo> StarVideos { get; set; }

        public StarDataModel()
        {
            StarVideos = new List<StarVideo>();
        }
    }
}