namespace Evo.WebApi.Models.Videos.JoinerTables
{
    public class StarVideo
    {
        public string VideoDataModelId { get; set; }
        public VideoDataModel VideoDataModel { get; set; }
        
        public int StarDataModelId { get; set; }
        public StarDataModel StarDataModel { get; set; }
    }
}