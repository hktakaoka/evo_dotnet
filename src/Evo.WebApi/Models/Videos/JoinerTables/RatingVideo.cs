namespace Evo.WebApi.Models.Videos.JoinerTables
{
    public class RatingVideo
    {
        public string VideoDataModelId { get; set; }
        public VideoDataModel VideoDataModel { get; set; }
        
        public int RatingDataModelId { get; set; }
        public RatingDataModel RatingDataModel { get; set; }
    }
}