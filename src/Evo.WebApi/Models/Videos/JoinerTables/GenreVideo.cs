namespace Evo.WebApi.Models.Videos.JoinerTables
{
    public class GenreVideo
    {
        public string VideoDataModelId { get; set; }
        public VideoDataModel VideoDataModel { get; set; }
        
        public int GenreDataModelId { get; set; }
        public GenreDataModel GenreDataModel { get; set; }
    }
}