using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Evo.WebApi.Models.Videos.JoinerTables;

namespace Evo.WebApi.Models.Videos
{
    public class RatingDataModel
    {
        public int RatingDataModelId { get; set; }
        [Required]
        public string Source { get; set; }
        [Required]
        public double RatingValue { get; set; }
        
        public ICollection<RatingVideo> RatingVideos { get; set; }

        public RatingDataModel()
        {
            RatingVideos = new List<RatingVideo>(); 
        }
    }
}