using System.ComponentModel.DataAnnotations;
using Evo.WebApi.Models.Videos.Enums;

namespace Evo.WebApi.Models.Requests
{
    public class TvEpisodeRequest : VideoRequest
    {
        public override VideoType Type => VideoType.TvShow;
        [Required]
        public string TvEpisodeId { get; set; }
        [Required]
        public int SeasonNumber { get; set; }
        [Required]
        public int EpisodeNumber { get; set; }
        public string EpisodeName { get; set; }
    }
}