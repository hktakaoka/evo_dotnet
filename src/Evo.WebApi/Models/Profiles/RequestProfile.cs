using System;
using AutoMapper;
using Evo.WebApi.Models.Requests;
using Evo.WebApi.Models.Videos;

namespace Evo.WebApi.Models.Profiles
{
    public class RequestProfile : Profile
    {
        public RequestProfile()
        {
            CreateMap<StarRequest, StarDataModel>()
                .ForMember(dest => dest.Role, source => source.MapFrom(m => m.Role))
                .ForMember(dest => dest.FirstName, source => source.MapFrom(m => m.FirstName))
                .ForMember(dest => dest.MiddleName, source => source.MapFrom(m => m.MiddleName))
                .ForMember(dest => dest.LastName, source => source.MapFrom(m => m.LastName))
                .ForMember(dest => dest.Suffix, source => source.MapFrom(m => m.Suffix))
                .ForMember(dest => dest.StarVideos, source => source.Ignore())
                .ForMember(dest => dest.StarDataModelId, source => source.Ignore());

            CreateMap<RatingRequest, RatingDataModel>()
                .ForMember(dest => dest.Source, source => source.MapFrom(m => m.Source))
                .ForMember(dest => dest.RatingValue, source => source.MapFrom(m => m.RatingValue))
                .ForMember(dest => dest.RatingVideos, source => source.Ignore())
                .ForMember(dest => dest.RatingDataModelId, source => source.Ignore());

            CreateMap<GenreRequest, GenreDataModel>()
                .ForMember(dest => dest.Name, source => source.MapFrom(m => m.Name))
                .ForMember(dest => dest.GenreVideos, source => source.Ignore())
                .ForMember(dest => dest.GenreDataModelId, source => source.Ignore());

            CreateMap<VideoRequest, VideoDataModel>()
                .ForMember(dest => dest.Codec, source => source.MapFrom(m => m.Codec))
                .ForMember(dest => dest.Plot, source => source.MapFrom(m => m.Plot))
                .ForMember(dest => dest.Resolution, source => source.MapFrom(m => m.Resolution))
                .ForMember(dest => dest.Runtime, source => source.MapFrom(m => m.Runtime))
                .ForMember(dest => dest.Title, source => source.MapFrom(m => m.Title))
                .ForMember(dest => dest.MpaaRating, source => source.MapFrom(m => m.MpaaRating))
                .ForMember(dest => dest.ReleaseDate, source => source.MapFrom(m => m.ReleaseDate))
                .ForMember(dest => dest.VideoType, source => source.MapFrom(m => m.Type))
                .ForMember(dest => dest.VideoDataModelId, source => source.MapFrom(m => m.VideoId))
                .AfterMap((_, dest) =>
                {
                    dest.Added = DateTime.Now;
                    dest.LastModified = DateTime.Now;
                });
        }        
    }
}