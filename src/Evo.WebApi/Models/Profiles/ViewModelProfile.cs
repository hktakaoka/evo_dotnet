using AutoMapper;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.ViewModels;

namespace Evo.WebApi.Models.Profiles
{
    public class ViewModelProfile : Profile
    {
        public ViewModelProfile()
        {
            CreateMap<VideoDataModel, Video>()
                .ForMember(dest => dest.Added, source => source.MapFrom(m => m.Added))
                .ForMember(dest => dest.Codec, source => source.MapFrom(m => m.Codec))
                .ForMember(dest => dest.Plot, source => source.MapFrom(m => m.Plot))
                .ForMember(dest => dest.Resolution, source => source.MapFrom(m => m.Resolution))
                .ForMember(dest => dest.Runtime, source => source.MapFrom(m => m.Runtime))
                .ForMember(dest => dest.Title, source => source.MapFrom(m => m.Title))
                .ForMember(dest => dest.Genres, source => source.MapFrom(m => m.GenreVideos))
                .ForMember(dest => dest.MpaaRating, source => source.MapFrom(m => m.MpaaRating))
                .ForMember(dest => dest.Ratings, source => source.MapFrom(m => m.RatingVideos))
                .ForMember(dest => dest.ReleaseDate, source => source.MapFrom(m => m.ReleaseDate))
                .ForMember(dest => dest.Stars, source => source.MapFrom(m => m.StarVideos))
                .ForMember(dest => dest.TvEpisodes, source => source.MapFrom(m => m.TvEpisodes))
                .ForMember(dest => dest.VideoId, source => source.MapFrom(m => m.VideoDataModelId))
                .ForMember(dest => dest.VideoType, source => source.MapFrom(m => m.VideoType));

            CreateMap<TvEpisodeDataModel, TvEpisode>()
                .ForMember(dest => dest.Codec, source => source.MapFrom(m => m.Codec))
                .ForMember(dest => dest.Plot, source => source.MapFrom(m => m.Plot))
                .ForMember(dest => dest.Resolution, source => source.MapFrom(m => m.Resolution))
                .ForMember(dest => dest.EpisodeNumber, source => source.MapFrom(m => m.EpisodeNumber))
                .ForMember(dest => dest.EpisodeName, source => source.MapFrom(m => m.EpisodeName))
                .ForMember(dest => dest.ReleaseDate, source => source.MapFrom(m => m.ReleaseDate))
                .ForMember(dest => dest.SeasonNumber, source => source.MapFrom(m => m.SeasonNumber))
                .ForMember(dest => dest.VideoId, source => source.MapFrom(m => m.VideoDataModelId))
                .ForMember(dest => dest.TvEpisodeId, source => source.MapFrom(m => m.TvEpisodeDataModelId));

            CreateMap<GenreDataModel, Genre>()
                .ForMember(dest => dest.Name, source => source.MapFrom(m => m.Name));

            CreateMap<RatingDataModel, Rating>()
                .ForMember(dest => dest.Source, source => source.MapFrom(m => m.Source))
                .ForMember(dest => dest.RatingValue, source => source.MapFrom(m => m.RatingValue));

            CreateMap<StarDataModel, Star>()
                .ForMember(dest => dest.Role, source => source.MapFrom(m => m.Role))
                .ForMember(dest => dest.Suffix, source => source.MapFrom(m => m.Suffix))
                .ForMember(dest => dest.FirstName, source => source.MapFrom(m => m.FirstName))
                .ForMember(dest => dest.LastName, source => source.MapFrom(m => m.LastName))
                .ForMember(dest => dest.MiddleName, source => source.MapFrom(m => m.MiddleName));
        }
    }
}