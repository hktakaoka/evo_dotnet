using AutoMapper;
using Evo.WebApi.Models.ViewModels;
using Evo.WebApi.Models.Videos.JoinerTables;

namespace Evo.WebApi.Models.Profiles
{
    public class JoinerTableProfile : Profile
    {
        public JoinerTableProfile()
        {
            CreateMap<StarVideo, Star>()
                .ForMember(dest => dest.Role, source => source.MapFrom(m => m.StarDataModel.Role))
                .ForMember(dest => dest.Suffix, source => source.MapFrom(m => m.StarDataModel.Suffix))
                .ForMember(dest => dest.FirstName, source => source.MapFrom(m => m.StarDataModel.FirstName))
                .ForMember(dest => dest.LastName, source => source.MapFrom(m => m.StarDataModel.LastName))
                .ForMember(dest => dest.MiddleName, source => source.MapFrom(m => m.StarDataModel.MiddleName));

            CreateMap<GenreVideo, Genre>()
                .ForMember(dest => dest.Name, source => source.MapFrom(m => m.GenreDataModel.Name));

            CreateMap<RatingVideo, Rating>()
                .ForMember(dest => dest.Source, source => source.MapFrom(m => m.RatingDataModel.Source))
                .ForMember(dest => dest.RatingValue, source => source.MapFrom(m => m.RatingDataModel.RatingValue));
        }
    }
}