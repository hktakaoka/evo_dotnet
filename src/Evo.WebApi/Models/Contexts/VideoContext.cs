using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.JoinerTables;
using Microsoft.EntityFrameworkCore;

namespace Evo.WebApi.Models.Contexts
{
    public class VideoContext : DbContext
    {
        public VideoContext(DbContextOptions<VideoContext> options)
            : base(options) {}
        
        public DbSet<VideoDataModel> Videos { get; set; }
        public DbSet<TvEpisodeDataModel> TvEpisodes { get; set; }
        public DbSet<StarDataModel> Stars { get; set; }
        public DbSet<GenreDataModel> Genres { get; set; }
        public DbSet<RatingDataModel> Ratings { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            SetCompositeKeys(modelBuilder);
            SetupJoinerTables(modelBuilder);
            SetupUniqueIndex(modelBuilder);
        }

        private static void SetupUniqueIndex(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StarDataModel>()
                .HasIndex(i => new {i.FirstName, i.LastName, i.MiddleName})
                .IsUnique();

            modelBuilder.Entity<GenreDataModel>()
                .HasIndex(i => i.Name)
                .IsUnique();

            modelBuilder.Entity<RatingDataModel>()
                .HasIndex(i => new {i.Source, i.RatingValue})
                .IsUnique();
        }

        private static void SetCompositeKeys(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TvEpisodeDataModel>()
                .HasKey(k => new {VideoId = k.VideoDataModelId, TvEpisodeId = k.TvEpisodeDataModelId});

            modelBuilder.Entity<GenreVideo>()
                .HasKey(k => new {GenreId = k.GenreDataModelId, VideoId = k.VideoDataModelId});

            modelBuilder.Entity<StarVideo>()
                .HasKey(k => new {VideoId = k.VideoDataModelId, StarId = k.StarDataModelId});

            modelBuilder.Entity<RatingVideo>()
                .HasKey(k => new {VideoId = k.VideoDataModelId, RatingId = k.RatingDataModelId});
        }

        private static void SetupJoinerTables(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GenreVideo>()
                .HasOne(gv => gv.VideoDataModel)
                .WithMany(video => video.GenreVideos)
                .HasForeignKey(gv => gv.VideoDataModelId);

            modelBuilder.Entity<GenreVideo>()
                .HasOne(gv => gv.GenreDataModel)
                .WithMany(genre => genre.GenreVideos)
                .HasForeignKey(gv => gv.GenreDataModelId);

            modelBuilder.Entity<StarVideo>()
                .HasOne(pv => pv.VideoDataModel)
                .WithMany(video => video.StarVideos)
                .HasForeignKey(pv => pv.VideoDataModelId);

            modelBuilder.Entity<StarVideo>()
                .HasOne(pv => pv.StarDataModel)
                .WithMany(person => person.StarVideos)
                .HasForeignKey(pv => pv.StarDataModelId);

            modelBuilder.Entity<RatingVideo>()
                .HasOne(rv => rv.VideoDataModel)
                .WithMany(video => video.RatingVideos)
                .HasForeignKey(rv => rv.VideoDataModelId);

            modelBuilder.Entity<RatingVideo>()
                .HasOne(rv => rv.RatingDataModel)
                .WithMany(video => video.RatingVideos)
                .HasForeignKey(rv => rv.RatingDataModelId);
        }
    }
}