﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.Profiles;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;

namespace Evo.WebApi
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration.GetConnectionString($"evo_{Configuration["ASPNETCORE_ENVIRONMENT"]}");
            
            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });

            services.AddDbContext<VideoContext>(
                options => options.UseMySql(connection))
                .AddSwaggerGen(options =>
                {
                    options.SwaggerDoc("v1", new Info
                    {
                        Title = "Evo API",
                        Version = "v1",
                        Contact = new Contact
                        {
                            Name = "Aaron Rinn",
                            Url = "https://gitlab.com/arinn1204/evo_dotnet"
                        },
                        License = new License
                        {
                            Name = "MIT",
                            Url = "https://gitlab.com/arinn1204/evo_dotnet/blob/master/LICENSE"
                        }
                    });
                })
                .AddAutoMapper(typeof(JoinerTableProfile).Assembly);

            var containerBuilder = new ContainerBuilder();
            
            containerBuilder.RegisterAssemblyTypes(GetType().Assembly)
                .Where(w => w.IsPublic && !w.IsInterface)
                .AsImplementedInterfaces();
            
            containerBuilder.Populate(services);
            var container = containerBuilder.Build();
                
            return new AutofacServiceProvider(container);
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Evo API v1");
                })
                .UseHttpsRedirection()
                .UseMvc();
        }
    }
}