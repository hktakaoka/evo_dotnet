using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Repositories.Interfaces;

namespace Evo.WebApi.Repositories
{
    public class StarRepository : IStarRepository
    {
        private readonly VideoContext _context;

        public StarRepository(VideoContext context)
        {
            _context = context;
        }
        
        public IEnumerable<StarDataModel> GetStars(int count = 25)
        {
            return _context.Stars
                .OrderBy(o => o.LastName)
                .Take(count);
        }

        public StarDataModel GetStar(string firstName, string lastName, string middleName)
        {
            return _context
                .Stars
                .FirstOrDefault(f =>
                    f.FirstName == firstName
                    && f.LastName == lastName
                    && f.MiddleName == middleName);
        }

        public async Task AddStar(StarDataModel starToAdd)
        {
            if (_context.Stars.Any(a => a.FirstName == starToAdd.FirstName
                                        && a.LastName == starToAdd.LastName
                                        && a.Suffix == starToAdd.Suffix
                                        && a.MiddleName == starToAdd.MiddleName))
            {
                return;
            }
            _context.Stars.Add(starToAdd);
            await _context.SaveChangesAsync();
        }
    }
}