using System;
using System.Linq;
using System.Threading.Tasks;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Repositories.Interfaces;

namespace Evo.WebApi.Repositories
{
    public class RatingRepository : IRatingRepository
    {
        private readonly VideoContext _context;

        public RatingRepository(VideoContext context)
        {
            _context = context;
        }

        public async Task AddRating(RatingDataModel rating)
        {
            if (_context.Ratings.Any(a => a.Source == rating.Source))
            {
                return;
            }
            _context.Ratings.Add(rating);
            await _context.SaveChangesAsync();
        }

        public RatingDataModel GetRating(string ratingSource, double ratingValue)
        {
            return _context.Ratings
            .FirstOrDefault(f => 
                f.Source == ratingSource 
                && Math.Abs(f.RatingValue - ratingValue) < 0.01);
        }
    }
}