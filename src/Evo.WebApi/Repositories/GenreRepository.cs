using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Repositories.Interfaces;

namespace Evo.WebApi.Repositories
{
    public class GenreRepository : IGenreRepository
    {
        private readonly VideoContext _context;

        public GenreRepository(VideoContext context)
        {
            _context = context;
        }

        public IEnumerable<GenreDataModel> GetGenres()
        {
            return _context
                .Genres
                .OrderBy(o => o.Name);
        }

        public GenreDataModel GetGenre(string genreName)
        {
            return _context
                .Genres
                .FirstOrDefault(s => s.Name == genreName);
        }

        public async Task AddGenre(GenreDataModel genreToAdd)
        {
            if (_context.Genres.Any(a => a.Name == genreToAdd.Name))
            {
                return;
            }
            _context.Genres.Add(genreToAdd);
            await _context.SaveChangesAsync();
        }
    }
}