using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Evo.WebApi.Repositories
{
    public class VideoRepository : IVideoRepository, IDisposable
    {
        private readonly VideoContext _context;

        public VideoRepository(VideoContext context)
        {
            _context = context;
        }
        public async Task<VideoDataModel> GetById(string imdbId)
        {
            var video = _context.Videos
                .Include(i => i.GenreVideos)
                    .ThenInclude(ti => ti.GenreDataModel)
                .Include(i => i.StarVideos)
                    .ThenInclude(ti => ti.StarDataModel)
                .Include(i => i.RatingVideos)
                    .ThenInclude(ti => ti.RatingDataModel)
                .Include(i => i.TvEpisodes)
                .FirstOrDefaultAsync(w => w.VideoDataModelId == imdbId);

            return await video;
        }

        public IEnumerable<VideoDataModel> GetAll()
        {
            var videos = _context.Videos
                .Include(i => i.GenreVideos)
                .ThenInclude(ti => ti.GenreDataModel)
                .Include(i => i.StarVideos)
                .ThenInclude(ti => ti.StarDataModel)
                .Include(i => i.RatingVideos)
                .ThenInclude(ti => ti.RatingDataModel)
                .Include(i => i.TvEpisodes);

            return videos.OrderBy(o => o.VideoDataModelId);
        }

        public async Task Delete(VideoDataModel videoToDelete)
        {
            _context.Videos.Remove(videoToDelete);
            await _context.SaveChangesAsync();
        }

        public async Task AddOrUpdate(VideoDataModel video)
        {
            if (_context.Videos.Any(a => a.VideoDataModelId == video.VideoDataModelId))
            {
                _context.Videos.Attach(video);
                _context.Entry(video).State = EntityState.Modified;
            }
            else
            {
                _context.Videos.Add(video);
            }
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}