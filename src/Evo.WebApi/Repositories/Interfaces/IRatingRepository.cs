using Evo.WebApi.Models.Videos;

namespace Evo.WebApi.Repositories.Interfaces
{
    public interface IRatingRepository
    {
        RatingDataModel GetRating(string ratingSource, double ratingValue);
    }
}