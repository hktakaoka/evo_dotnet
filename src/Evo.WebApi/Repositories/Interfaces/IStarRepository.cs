using System.Collections.Generic;
using Evo.WebApi.Models.Videos;

namespace Evo.WebApi.Repositories.Interfaces
{
    public interface IStarRepository
    {
        IEnumerable<StarDataModel> GetStars(int count = 25);
        StarDataModel GetStar(string firstName, string lastName, string middleName);
    }
}