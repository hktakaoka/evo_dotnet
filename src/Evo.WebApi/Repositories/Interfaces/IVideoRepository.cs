using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Evo.WebApi.Models.Videos;

namespace Evo.WebApi.Repositories.Interfaces
{
    public interface IVideoRepository
    {
        Task<VideoDataModel> GetById(string imdbId);
        IEnumerable<VideoDataModel> GetAll();
        Task Delete(VideoDataModel videoToDelete);
        Task AddOrUpdate(VideoDataModel video);
    }
}