using System.Collections.Generic;
using Evo.WebApi.Models.Videos;

namespace Evo.WebApi.Repositories.Interfaces
{
    public interface IGenreRepository
    {
        IEnumerable<GenreDataModel> GetGenres();
        GenreDataModel GetGenre(string genreName);
    }
}