using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Evo.WebApi.Exceptions;
using Evo.WebApi.Models.Requests;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.ViewModels;
using Evo.WebApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Evo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class VideosController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IVideoService _videoService;

        public VideosController(IVideoService videoService, IMapper mapper)
        {
            _videoService = videoService;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Video>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public IActionResult GetVideos()
        {
            IActionResult result;

            try
            {
                var videos = _videoService.GetVideos();
                result = Ok(videos.Select(_mapper.Map<Video>));
            }
            catch (Exception e)
            {
                result = StatusCode(StatusCodes.Status500InternalServerError, new ErrorResponse
                {
                    Error = e.Message
                });
            }

            return result;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Video), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetVideoWithId(string id)
        {
            IActionResult result;
            try
            {
                var videoDataModel = await _videoService.GetById(id);
                result = Ok(_mapper.Map<Video>(videoDataModel));
            }
            catch (EvoNotFoundException)
            {
                result = new NotFoundResult();
            }
            catch (Exception e)
            {
                result = StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new ErrorResponse
                    {
                        Error = e.Message
                    });
            }


            return result;
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteById(string id)
        {
            try
            {
                await _videoService.DeleteById(id);
            }
            catch (EvoNotFoundException)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new ErrorResponse
                {
                    Error = e.Message
                });
            }

            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(BadRequest), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddVideo([FromBody] VideoRequest request)
        {
            VideoDataModel result;
            try
            {
                result = await _videoService.AddOrUpdate(request);
            }
            catch (EvoBadRequestException e)
            {
                var badRequest = new BadRequest
                {
                    Id = request.VideoId,
                    Message = e.Message
                };

                return new BadRequestObjectResult(badRequest);
            }

            var viewModel = _mapper.Map<Video>(result);
            return Created($"/videos/{request.VideoId}", viewModel);
        }
    }
}