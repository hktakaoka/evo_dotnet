using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.Enums;
using Evo.WebApi.Models.ViewModels;
using Evo.WebApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Evo.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StarsController : Controller
    {
        private readonly IStarService _starService;
        private readonly IMapper _mapper;

        public StarsController(IStarService starService, IMapper mapper)
        {
            _starService = starService;
            _mapper = mapper;
        }

        [HttpGet("{role}")]
        [ProducesResponseType(typeof(IEnumerable<Star>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        public IActionResult GetStars(string role)
        {
            if (!Enum.TryParse<PersonType>(role.Trim('s'), true, out var personType))
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    Error = $"{role} is an unknown star type."
                });
            }

            IEnumerable<StarDataModel> stars;
            try
            {
                stars = _starService.GetStars(personType);
            }
            catch (Exception e)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new ErrorResponse
                    {
                        Error = e.Message
                    });
            }

            return Ok(stars.Select(_mapper.Map<Star>));
        }
    }
}