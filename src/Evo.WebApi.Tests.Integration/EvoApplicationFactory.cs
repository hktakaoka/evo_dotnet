using Evo.WebApi.Models.Contexts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Evo.WebApi.Tests.Integration
{
    public class EvoApplicationFactory<TStartup> : WebApplicationFactory<TStartup>
    where TStartup : class
    {
        private readonly SqliteConnection _connection;

        public EvoApplicationFactory(SqliteConnection connection)
        {
            _connection = connection;
        }
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
                {
                    services.AddDbContext<VideoContext>(options => options.UseSqlite(_connection));
                });
        }
    }
}