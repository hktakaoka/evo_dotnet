Feature: ErrorStates

  @error_404
  Scenario Outline: A user wants to interact with an unknown item
    Given a user wants an unknown <entity>
    When the user interacts with the api
    Then the user is alerted that the <entity> does not exist
    
    Examples:
      | entity    |
      | video    |
      | actor    |
      | writer   |
      | director |
      | producer |
    
  @error_400
  Scenario: A user wants to access an unknown type of star
    Given a user wants an incorrect star
    When the user interacts with the api
    Then the user is alerted that it was a bad request

  @error_400
  Scenario: A user attempts to create a new video for an existing ID
    Given a user that wants to create a new video
    But the video already exists
    When the user interacts with the api
    Then the user is alerted that it was a bad request
    And the user is alerted that the video already exists
    
  @error_404
  Scenario: A user wants to delete an unknown item
    Given a user wants to delete an unknown video
    When the user interacts with the api
    Then the user is alerted that the video does not exist