using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.Videos.Enums;
using Evo.WebApi.Tests.Helpers.Extensions;
using Evo.WebApi.Tests.Helpers.Models.Database;
using Microsoft.EntityFrameworkCore;
using TechTalk.SpecFlow;

namespace Evo.WebApi.Tests.Integration.Features.Steps.Support
{
    [Binding]
    public class DatabaseInitializer
    {
        private readonly DbContextOptions<VideoContext> _options;

        public DatabaseInitializer(DbContextOptions<VideoContext> options)
        {
            _options = options;
        }

        [BeforeScenario]
        public void SetupDatabaseEntry()
        {
            using (var context = new VideoContext(_options))
            {
                context.Database.EnsureCreated();

                var video = Fakes.GetVideo("tt1234", "Donnie Darko");
                video.AddJoinerTable(Fakes.GetStar("Jake", lastName: "Gyllenhaal"));
                video.AddJoinerTable(Fakes.GetStar("James", lastName: "Cameron", role: PersonType.Producer));
                video.AddJoinerTable(Fakes.GetStar("James", lastName: "Nolan", role: PersonType.Director));
                video.AddJoinerTable(Fakes.GetStar("Richard", lastName: "Kelly", role: PersonType.Writer));
                video.AddJoinerTable(Fakes.GetGenre("Horror"));
                video.AddJoinerTable(Fakes.GetRating("Metacritic"));

                context.Videos.Add(video);

                context.SaveChanges();
            }
        }

        [AfterScenario]
        public void DestroyDatabaseEntries()
        {
            using (var context = new VideoContext(_options))
            {
                context.Videos.RemoveRange(context.Videos);
                context.Stars.RemoveRange(context.Stars);
                context.Ratings.RemoveRange(context.Ratings);
                context.Genres.RemoveRange(context.Genres);
                context.TvEpisodes.RemoveRange(context.TvEpisodes);

                context.Database.EnsureDeleted();
                context.SaveChanges();
            }
        }
    }
}