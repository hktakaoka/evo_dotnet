using System;
using System.Net.Http;
using System.Threading.Tasks;
using BoDi;
using Evo.WebApi.Models.Contexts;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using TechTalk.SpecFlow;

namespace Evo.WebApi.Tests.Integration.Features.Steps.Support
{
    [Binding]
    public class Hooks
    {
        [BeforeTestRun]
        public static async Task TestRunSetup(IObjectContainer container)
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Test");

            var connection = new SqliteConnection("DataSource=:memory:");
            var factory = new EvoApplicationFactory<Startup>(connection);

            connection.Open();
            container.RegisterInstanceAs(connection);
            container.RegisterInstanceAs(factory);

            var options = new DbContextOptionsBuilder<VideoContext>()
                .UseSqlite(connection)
                .Options;

            container.RegisterInstanceAs(options);
        }

        [BeforeFeature]
        public static void BeforeFeature(IObjectContainer container, EvoApplicationFactory<Startup> appFactory)
        {
            var client = appFactory.CreateClient();
            container.RegisterInstanceAs(client);
        }

        [AfterFeature]
        public static void AfterFeature(IObjectContainer container)
        {
            var client = container.Resolve<HttpClient>();
            client.Dispose();
        }

        [AfterTestRun]
        public static void AfterTestRun(IObjectContainer container)
        {
            var factory = container.Resolve<EvoApplicationFactory<Startup>>();
            var connection = container.Resolve<SqliteConnection>();
            connection.Close();
            connection.Dispose();
            factory.Dispose();
        }
    }
}