using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoBogus;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.Requests;
using Evo.WebApi.Tests.Helpers.Database;
using Evo.WebApi.Tests.Helpers.Models.Database;
using Evo.WebApi.Tests.Helpers.Models.Requests;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using TechTalk.SpecFlow;

namespace Evo.WebApi.Tests.Integration.Features.Steps.RequestBuilder
{
    [Binding]
    public class RequestBuilder
    {
        private const string IdToModify = "tt47589312";
        private const string UnknownId = "ttYouDontKnowMe";
        private const string IdToDelete = "deleteMePls";
        private readonly Models.Evo _evo;
        private readonly DbContextOptions<VideoContext> _options;
        private readonly IDictionary<string, object> _requestBody;

        private readonly IDictionary<string, string> _serviceMapping;

        public RequestBuilder(Models.Evo evo, DbContextOptions<VideoContext> options)
        {
            _evo = evo;
            _evo.RequestMessage = new HttpRequestMessage();
            _options = options;

            _serviceMapping = new Dictionary<string, string>
            {
                {"genres", "api/genres"},
                {"videos", "api/videos"},
                {"actors", "api/stars/actors"},
                {"writers", "api/stars/writers"},
                {"directors", "api/stars/directors"},
                {"producers", "api/stars/producers"},
                {"tv episodes", "api/tv_episodes"},
                {"ratings", "api/ratings"}
            };

            _requestBody = new Dictionary<string, object>
            {
                {"genres", new AutoFaker<GenreRequest>().UseSeed(1).Generate()},
                {"actors", new AutoFaker<StarRequest>().UseSeed(1).Generate()},
                {"writers", new AutoFaker<StarRequest>().UseSeed(1).Generate()},
                {"directors", new AutoFaker<StarRequest>().UseSeed(1).Generate()},
                {"producers", new AutoFaker<StarRequest>().UseSeed(1).Generate()},
                {"ratings", new AutoFaker<RatingRequest>().UseSeed(1).Generate()},
                {"videos", FakeVideoRequests.CreateVideoRequest(IdToModify)},
                {"tv episodes", FakeTvEpisodeRequests.CreateTvEpisode(IdToModify, $"{IdToModify}1")}
            };
        }

        [Given(@"a user that wants to see available (.*)")]
        public void GivenAUserThatWantsToSeeAvailableEntries(string entry)
        {
            var serviceUri = $"{_evo.BaseAddress}{_serviceMapping[entry]}";
            _evo.RequestMessage.RequestUri = new Uri(serviceUri);
            _evo.RequestMessage.Method = HttpMethod.Get;
            _evo.RequestMessage.Content = null;
        }

        [Given(@"a user that wants to create a new (.*)")]
        public void GivenAUserThatWantsToCreateANewEntry(string entry)
        {
            var serviceUri = $"{_evo.BaseAddress}{_serviceMapping[$"{entry}s"]}";
            _evo.RequestMessage.RequestUri = new Uri(serviceUri);

            var requestString = JsonConvert.SerializeObject(_requestBody[$"{entry}s"]);
            var stringContent = new StringContent(requestString,
                Encoding.UTF8,
                "application/json");

            _evo.RequestMessage.Method = HttpMethod.Post;
            _evo.RequestMessage.Content = stringContent;
        }

        [Given(@"a user that wants to update an? (.*)")]
        public void GivenAUserThatWantsToUpdateAnEntry(string entry)
        {
            var serviceUri = $"{_evo.BaseAddress}{_serviceMapping[$"{entry}s"]}/{IdToModify}";
            _evo.RequestMessage.RequestUri = new Uri(serviceUri);

            var requestString = JsonConvert.SerializeObject(_requestBody[$"{entry}s"]);
            var stringContent = new StringContent(requestString,
                Encoding.UTF8,
                "application/json");

            _evo.RequestMessage.Method = HttpMethod.Put;
            _evo.RequestMessage.Content = stringContent;
        }

        [Given(@"a user that wants to delete a (.*)")]
        public async Task GivenAUserThatWantsToDeleteAEntry(string entry)
        {
            if (entry == "video") await DatabaseHelpers.AddVideo(IdToDelete, _options);

            var serviceUri = $"{_evo.BaseAddress}{_serviceMapping[$"{entry}s"]}/{IdToDelete}";
            _evo.RequestMessage.RequestUri = new Uri(serviceUri);

            _evo.RequestMessage.Method = HttpMethod.Delete;
            _evo.RequestMessage.Content = null;
        }

        [Given(@"a user wants an unknown (.*)")]
        public void GivenAUserWantsAnUnknownEntry(string entry)
        {
            var serviceUri = $"{_evo.BaseAddress}{_serviceMapping[$"{entry}s"]}/{UnknownId}";
            _evo.RequestMessage.RequestUri = new Uri(serviceUri);
            ;
            _evo.RequestMessage.Method = HttpMethod.Get;
            _evo.RequestMessage.Content = null;
        }

        [Given(@"a user wants an incorrect (.+)")]
        public void GivenAUserWantsAnIncorrectThing(string entity)
        {
            var serviceUri = _evo.BaseAddress + $"{_serviceMapping["actors"]}foobar";
            _evo.RequestMessage.RequestUri = new Uri(serviceUri);

            _evo.RequestMessage.Method = HttpMethod.Get;
            _evo.RequestMessage.Content = null;
        }

        [Given(@"a user wants to delete an unknown video")]
        public void GivenAUserWantsToDeleteAnUnknownVideo()
        {
            var serviceUri = $"{_evo.BaseAddress}{_serviceMapping["videos"]}/{UnknownId}";
            _evo.RequestMessage.RequestUri = new Uri(serviceUri);

            _evo.RequestMessage.Method = HttpMethod.Delete;
            _evo.RequestMessage.Content = null;
        }

        [Given(@"the video already exists")]
        public void ButThatVideoAlreadyExists()
        {
            var video = Fakes.GetVideo("tt47589312", "Some Title");
            using (var context = new VideoContext(_options))
            {
                context.Videos.Add(video);
                context.SaveChanges();
            }
        }
    }
}