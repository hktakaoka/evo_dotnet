using System.Net;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Evo.WebApi.Tests.Integration.Features.Steps.Assertions
{
    [Binding]
    public class StatusCodeValidation
    {
        private readonly Models.Evo _evo;

        public StatusCodeValidation(Models.Evo evo)
        {
            _evo = evo;
        }

        [Then(@"the user is alerted that it was a bad request")]
        public void ThenTheUserIsAlertedThatItWasABadRequest()
        {
            Assert.That(_evo.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Then(@"the user is alerted that it is created")]
        public void ThenTheUserIsAlertedThatItIsCreated()
        {
            Assert.That(_evo.StatusCode, Is.EqualTo(HttpStatusCode.Created));
        }

        [Then(@"the user is told it was successful")]
        public void ThenTheUserIsToldItWasSuccessful()
        {
            Assert.That(_evo.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Then(@"the user is alerted that the (.*) does not exist")]
        public void ThenTheUserIsAlertedThatTheGenresDoesNotExist(string entry)
        {
            Assert.That(_evo.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
        }
    }
}