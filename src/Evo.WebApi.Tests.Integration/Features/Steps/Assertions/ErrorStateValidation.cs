using Evo.WebApi.Models.ViewModels;
using FluentAssertions;
using Newtonsoft.Json;
using TechTalk.SpecFlow;

namespace Evo.WebApi.Tests.Integration.Features.Steps.Assertions
{
    [Binding]
    public class ErrorStateValidation
    {
        private readonly Models.Evo _evo;

        public ErrorStateValidation(Models.Evo evo)
        {
            _evo = evo;
        }

        [Then(@"the user is alerted that the video already exists")]
        public void TheUserIsAlertedThatTheVideoAlreadyExists()
        {
            var response = JsonConvert.DeserializeObject<BadRequest>(_evo.Response);

            var id = response.Id;
            var message = response.Message;

            id.Should()
                .BeEquivalentTo("tt47589312");

            message.Should()
                .BeEquivalentTo("Resource tt47589312 already exists.");
        }
    }
}