﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.ViewModels;
using Evo.WebApi.Tests.Helpers.Database;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Evo.WebApi.Tests.Integration.Features.Steps.Assertions
{
    [Binding]
    public class ServiceVerification
    {
        private readonly Models.Evo _evo;
        private readonly DbContextOptions<VideoContext> _options;

        public ServiceVerification(Models.Evo evo, DbContextOptions<VideoContext> options)
        {
            _evo = evo;
            _options = options;
        }

        [Then(@"the user is given a list of all the (.*)")]
        public void ThenTheUserIsGivenAListOfAllTheGenres(string entry)
        {
            var entries = JsonConvert.DeserializeObject<dynamic[]>(_evo.Response);

            Assert.That(
                entries,
                Is.Not.Null,
                "Expected a list of entries in the response");

            Assert.That(
                entries.Count,
                Is.GreaterThan(0),
                "Expected some items to be in the list");
        }

        [Then(@"the user is alerted that the video has been deleted")]
        public async Task ThenTheUserIsAlertedThatTheEntityHasBeenDeleted()
        {
            Assert.That(_evo.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));
            Assert.That(
                await DatabaseHelpers.VideoExists(_evo.RequestMessage.RequestUri.AbsolutePath.Split("/").Last(),
                    _options),
                Is.False);
        }

        [Then(@"the user receives a copy of the (?:new|deleted) (.*)")]
        public void ThenTheUserReceivesACopyOfTheDeletedGenres(string entry)
        {
            var typedAssembly = typeof(Video).Assembly;
            var entryType =
                typedAssembly.GetType($"Evo.WebApi.Models.ViewModels.{char.ToUpper(entry[0]) + entry.Substring(1)}");
            var response = JsonConvert.DeserializeObject(_evo.Response, entryType);
            Assert.That(response, Is.Not.Null);
            Assert.That(response.GetType()
                    .GetProperties()
                    .First(w => w.Name.Contains("Id")),
                Is.Not.Null);
        }

        [Then(@"the videos are sorted by latest")]
        public void ThenTheVideosAreSortedByLatest()
        {
            var videos = JsonConvert.DeserializeObject<List<dynamic>>(_evo.Response);
            Assert.That(videos.First().Added, Is.GreaterThan(videos.Last().Added));
        }
    }
}