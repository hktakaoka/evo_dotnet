using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Evo.WebApi.Tests.Integration.Extensions;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Evo.WebApi.Tests.Integration.Features.Steps
{
    [Binding]
    public class ServiceCaller
    {
        private readonly HttpClient _client;
        private readonly Models.Evo _evo;


        public ServiceCaller(Models.Evo evo, HttpClient client)
        {
            _evo = evo;
            _client = client;
        }

        [When(@"the user (successfully)?\s{0,1}interacts with the api")]
        public async Task WhenTheUserAccessesTheApi(string successfully)
        {
            var response = await _client.SendAsync(_evo.RequestMessage);
            var content = await response.GetContent();
            _evo.StatusCode = response.StatusCode;
            _evo.Response = content;
            if (successfully == "successfully")
                Assert.That(
                    response.StatusCode,
                    Is.EqualTo(HttpStatusCode.OK),
                    $"The following is the message from the service:: {content}");
        }
    }
}