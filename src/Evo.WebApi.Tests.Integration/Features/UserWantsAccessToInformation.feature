﻿Feature: User Wants To Access To Information

	@all_endpoints
	Scenario Outline: A user wants to see the available entries
      Given a user that wants to see available <entry>
      When the user successfully interacts with the api
      Then the user is given a list of all the <entry>

	  Examples:
	  | entry          |
	  | genres         |
	  | producers      |
	  | actors         |
	  | writers        |
	  | directors      |
	  | videos         |
