using System.Linq;
using System.Reflection;
using AutoMapper;
using Evo.WebApi.Models.Profiles;

namespace Evo.WebApi.Tests.Helpers.Models.Mapper
{
    public static class CreateMapper
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(
                cfg => 
                    cfg.AddMaps(
                        Enumerable.Empty<Assembly>()
                            .Append(typeof(JoinerTableProfile).Assembly)));
            
            return new AutoMapper.Mapper(config);
        }
    }
}