using System;
using System.Collections.Generic;
using System.Linq;
using AutoBogus;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.Enums;
using Evo.WebApi.Models.Videos.JoinerTables;

namespace Evo.WebApi.Tests.Helpers.Models.Database
{
    public static class Fakes
    {
        private static int _counter = 0;
        public static VideoDataModel GetVideo(string videoId, string title = "") =>
            new AutoFaker<VideoDataModel>()
                .UseSeed(1)
                .RuleFor(r => r.VideoDataModelId, r => videoId)
                .RuleFor(r => r.StarVideos, r => new List<StarVideo>())
                .RuleFor(r => r.RatingVideos, r => new List<RatingVideo>())
                .RuleFor(r => r.TvEpisodes, r => new List<TvEpisodeDataModel>())
                .RuleFor(r => r.GenreVideos, r => new List<GenreVideo>())
                .RuleFor(r => r.Added, r => DateTime.Now)
                .RuleFor(r => r.Plot, r => r.Name.JobDescriptor())
                .RuleFor(r => r.Title, r => string.IsNullOrWhiteSpace(title) ? r.Lorem.Word() : title)
                .Generate();

        public static StarDataModel GetStar(
            string firstName,
            int id = -1,
            PersonType role = PersonType.Actor,
            string lastName = "Smith",
            IEnumerable<StarVideo> videos = null) =>
                new AutoFaker<StarDataModel>()
                    .UseSeed(1)
                    .RuleFor(r => r.FirstName, r => firstName)
                    .RuleFor(r => r.LastName, r => lastName)
                    .RuleFor(r => r.StarVideos, r => videos?.ToList() ?? new List<StarVideo>())
                    .RuleFor(r => r.StarDataModelId, r => id == -1 ? ++_counter : id)
                    .RuleFor(r => r.Role, r => role)
                    .Generate();

        public static RatingDataModel GetRating(string sourceName, int id = -1) =>
            new AutoFaker<RatingDataModel>()
                .UseSeed(1)
                .RuleFor(r => r.Source, r => sourceName)
                .RuleFor(r => r.RatingVideos, r => new List<RatingVideo>())
                .RuleFor(r => r.RatingDataModelId, r => id == -1 ? ++_counter : id)
                .Generate();
        
        public static GenreDataModel GetGenre(string name, int id = -1) =>
            new AutoFaker<GenreDataModel>()
                .UseSeed(1)
                .RuleFor(r => r.Name, r => name)
                .RuleFor(r => r.GenreVideos, r => new List<GenreVideo>())
                .RuleFor(r => r.GenreDataModelId, r => id == -1 ? ++_counter : id)
                .Generate();

        public static TvEpisodeDataModel GetEpisode(string episodeId, VideoDataModel videoDataModel) =>
            new AutoFaker<TvEpisodeDataModel>()
                .UseSeed(1)
                .RuleFor(r => r.VideoDataModelId, videoDataModel.VideoDataModelId)
                .RuleFor(r => r.VideoDataModel, r => videoDataModel)
                .RuleFor(r => r.TvEpisodeDataModelId, r => episodeId)
                .Generate();
    }
}