using System;
using System.Collections.Generic;
using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.JoinerTables;

namespace Evo.WebApi.Tests.Helpers.Extensions
{
    public static class VideoExtensions
    {
        public static VideoDataModel AddJoinerTable<T>(this VideoDataModel @this, params T[] itemsToAdd)
        {
            var mapper = new Dictionary<Type, Type>
            {
                [typeof(GenreDataModel)] = typeof(GenreVideo),
                [typeof(StarDataModel)] = typeof(StarVideo),
                [typeof(RatingDataModel)] = typeof(RatingVideo)
            };
            
            foreach (var item in itemsToAdd)
            {
                var itemType = item.GetType();
                var joinerType = mapper[itemType];
                var joiner = Activator.CreateInstance(joinerType);
                var itemId = item
                    .GetType()
                    .GetProperty($"{itemType.Name}Id")
                    .GetValue(item);
                
                joiner.SetProperty("VideoDataModel", @this);
                joiner.SetProperty("VideoDataModelId", @this.VideoDataModelId);
                joiner.SetProperty(itemType.Name, item);
                joiner.SetProperty($"{itemType.Name}Id", itemId);
                
                var collection = @this
                    .GetType()
                    .GetProperty($"{joinerType.Name}s")
                    .GetValue(@this);

                collection
                    .GetType()
                    .GetMethod("Add")
                    .Invoke(collection, new[] { joiner });
            }

            return @this;
        }

        private static void SetProperty(this object @this, string property, object value)
        {
            @this.GetType()
                .GetProperty(property)
                .SetValue(@this, value);
        }
    }
}