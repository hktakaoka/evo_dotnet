using System.Linq;
using System.Threading.Tasks;
using Evo.WebApi.Models.Contexts;
using Evo.WebApi.Models.Videos;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace Evo.WebApi.Tests.Helpers.Database
{
    public static class DatabaseHelpers
    {
        public static async Task<bool> VideoExists(string id, DbContextOptions<VideoContext> options)
        {
            VideoDataModel video;
            using (var context = new VideoContext(options))
            {
                video = await context.Videos.FirstOrDefaultAsync(f => f.VideoDataModelId == id);
            }

            return video != null;
        }

        public static async Task AddVideo(string id, DbContextOptions<VideoContext> options)
        {
            using (var context = new VideoContext(options))
            {
                var video = Models.Database.Fakes.GetVideo(id);
                context.Videos.Add(video);
                await context.SaveChangesAsync();
            }
        }
    }
}