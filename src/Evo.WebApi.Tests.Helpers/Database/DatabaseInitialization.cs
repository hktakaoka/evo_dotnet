using Evo.WebApi.Models.Videos;
using Evo.WebApi.Models.Videos.Enums;
using Evo.WebApi.Models.Videos.JoinerTables;
using Evo.WebApi.Tests.Helpers.Models.Database;

namespace Evo.WebApi.Tests.Helpers.Database
{
    public static class DatabaseInitialization
    {
        public static VideoDataModel CreateHorrorMovieWithRatingsAndOneActor(string videoId, string videoTitle = "")
        {
            var videoToAdd = Fakes.GetVideo(videoId, videoTitle);
            videoToAdd.VideoType = VideoType.Video;
            
            var nancy = new StarVideo 
                {StarDataModel = Fakes.GetStar("Nancy"), StarDataModelId = 1, VideoDataModel = videoToAdd, VideoDataModelId = videoId};
            var horror = new GenreVideo
                {VideoDataModel = videoToAdd, VideoDataModelId = videoId, GenreDataModel = Fakes.GetGenre("Horror"), GenreDataModelId = 1};
            var metacritic = new RatingVideo
                {VideoDataModel = videoToAdd, VideoDataModelId = videoId, RatingDataModel = Fakes.GetRating("RottenTomatoes", 2), RatingDataModelId = 1};
            
            videoToAdd.StarVideos.Add(nancy);
            videoToAdd.GenreVideos.Add(horror);
            videoToAdd.RatingVideos.Add(metacritic);

            return videoToAdd;
        }
    }
}